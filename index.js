/**
 * @format
 */
import React, { Component } from 'react'
import {AppRegistry} from 'react-native';
import App from './src/App';
import {name as appName} from './app.json';
import { Provider } from 'react-redux';
import store from './src/store'


export default class Application extends Component {
    render() {
        return (
            
            <Provider store={store}>
                <App />
            </Provider>
        )
    }
}
AppRegistry.registerComponent(appName, () => Application);

