import {
    LOGIN_FAILED,
    LOGIN_SUCCESS,
    LOGIN_USER,
    LOG_OUT,
    LOGIN_SESSION,
    SET_USER_TYPE,
    UPDATE_LOCALS,
    UPDATE_FAILED,
    CLEAR_LOGIN_ERROR,
    UPDATE_SUCCESS
} from '../actions/types'

const initialState = {
    isAuthenticated: false,
    user: null,
    error: null,
    isLogging: false,
    updateStatus: null,
    loginMsg: undefined,
    userType: 'user',
    local: 'English'
}

export default authReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_USER:
            return {
                ...state,
                isLogging: true
            }
        case LOGIN_SUCCESS:
            return {
                ...state,
                isAuthenticated: true,
                user: action.user,
                loginMsg: action.msg,
                isLogging: false
            }
        case LOGIN_FAILED:
            return {
                ...state,
                isAuthenticated: false,
                error: action.error,
                isLogging: false
            }
        case CLEAR_LOGIN_ERROR:
            return {
                ...state,
                error: null,
                updateStatus: null
            }
        case UPDATE_SUCCESS:
            return {
                ...state,
                isLogging: false,
                updateStatus: true
            }
        case UPDATE_FAILED:
            return {
                ...state,
                isLogging: false,
                updateStatus: false
            }
        case LOG_OUT:
            return {
                isAuthenticated: false,
                user: null
            }
        case LOGIN_SESSION:
            const isAuthenticated = action.user !== null
            return {
                ...state,
                isAuthenticated: isAuthenticated,
                user: action.user,
                userType: action.userType,
                local: action.local
            }
        case SET_USER_TYPE:
            return {
                ...state,
                userType: action.userType
            }
        case UPDATE_LOCALS:
            return {
                ...state,
                local: action.local
            }
        default:
            return state
    }
}