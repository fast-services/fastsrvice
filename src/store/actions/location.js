import {
    UPDATE_CURRENT_LOCATION
} from './types'


export const updateCurrentLocation = (region) => {
    return {
        type: UPDATE_CURRENT_LOCATION,
        region
    }
}