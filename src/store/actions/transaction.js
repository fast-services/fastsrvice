import axios from '../../Server/DefaultServer'
import {
    ADD_PRICE, BILL_READY, PRICE_ADDED
} from './types'

export const addPrice = (price) => {
    return {
        type: ADD_PRICE,
        price: price
    }
}

export const startWalk = (region, socket) => async (dispatch, getState) => {
    try {
        const { id, token } = getState().auth.user
        const request_id = getState().service.request.request_id
        const price = getState().transation.price
        const { latitude, longitude } = region
        const url = `/provider/requestwalkstarted?id=${id}&token=${token}&request_id=${request_id}&latitude=${latitude}&longitude=${longitude}&base_price=${price}`
        const response = await axios.post(url)
        console.log({
            id,
            token,
            price,
            request_id,
            latitude,
            longitude
        })
        console.log('Walker started', response.data)
        if (response.data.success) {
            socket.emit('serviceCharge', id, price)
            dispatch({
                type: PRICE_ADDED
            })
        }
    } catch (err) {
        console.log('Error in sending price', err.message)
    }
}

export const workComplete = (region, navigation, socket) => async (dispatch, getState) => {
    try {
        const { id, token } = getState().auth.user
        const request_id = getState().service.request.request_id
        const { latitude, longitude } = region
        const base_price = getState().transation.price
        const distance = getState().service.request.distance
        const time = 'No time'
        const body = {
            id,
            token,
            latitude,
            longitude,
            request_id,
            base_price,
            time,
            distance
        }
        console.log(body)
        const response = await axios.post('/provider/requestwalkcompleted', body)
        console.log('Work Done', response.data)
        if (response.data.success) {
            console.log('Work Done')

            dispatch({
                type: BILL_READY,
                bill: response.data.bill,
            })
            socket.emit('workDone', request_id)
            
            navigation.navigate('Invoice')
        } else {
            console.log('Error in Work Done')
        }
    } catch (err) {
        console.log('Error in work complete', err)
    }
}

