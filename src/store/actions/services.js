import {
    FETCH_SERVICES,
    FETCH_SUB_SERVICES,
    SERVICE_FETCHING_ERROR,
    CREATE_REQUEST,
    SELECT_SERVICE,
    REQUEST_SEND,
    REQUEST_DECLINE,
    REQUEST_ACCEPT,
    REQUEST_PROVIDER_NOT_FOUND,
    REQUEST_RECEIVED,
    NO_REQUEST_RECEIVED,
    REQUEST_DECLINED,
    REQUEST_ACCEPTED,
    REQUEST_COMPLETED,
    REQUEST_PRICE,
    UPDATE_WALKER_CORDS,
    REQUEST_REJECTED,
    RATING_DONE,
    UPDATE_REQUEST_ID,
    CLEAR_PENDING_REQUEST,
    PENDING_REQUEST_PROVIDER,
    UPDATE_DISANCE_AND_TIME,
    HISTORY_FETCHED,
    UPDATE_WALKERS,
    TOGGLE_SERVICE_SELECTION,
    RESERVATIONLIST
} from './types'
import { REQUEST_TIME_OUT_SEC } from '../../utils/constants'
import { ToastAndroid } from 'react-native'
import {locals} from '../../utils/locals'
import axios from '../../Server/DefaultServer'
import * as firebase from 'firebase'

export const fetchServices = () => async (dispatch) => {
    try {
        const response = await axios.get('/application/types')
        dispatch({
            type: FETCH_SERVICES,
            services: response.data.types
        })
    } catch (err) {
        dispatch({
            type: SERVICE_FETCHING_ERROR,
            error: err.message
        })
    }
}

export const fetchSubServices = (id) => async (dispatch, getState) => {
    if (getState().service.subServices[id]) {
        return
    }
    try {
        const response = await axios.get(`/application/subtypes?id=${id}`)
        dispatch({
            type: FETCH_SUB_SERVICES,
            id: id,
            services: response.data.types
        })
        console.log('Fetched services')
    } catch (err) {
        console.log('Fetching sub services error')
        dispatch({
            type: SERVICE_FETCHING_ERROR,
            error: err.message
        })
    }
}

export const updateLatLog = (region) => async (dispatch, getState) => {
    try {
        const local = locals[getState().auth.local].toast
        const user = getState().auth.user
        const body = {
            id: user.id,
            token: user.token,
            latitude: region.latitude.toString(),
            longitude: region.longitude.toString()
        }

        const response = await axios.post(`/provider/location?id=${body.id}&token=${body.token}&latitude=${body.latitude}&longitude=${body.longitude}`)
        console.log(response.data)

        if (response.data.success) {
            ToastAndroid.show(local.locationUpdated, ToastAndroid.LONG)
        } else {
            ToastAndroid.show(local.locatonUpdatateFailed, ToastAndroid.LONG)
        }
    } catch (err) {
        console.log("Error in updating lat lng", err.message)
    }
}

export const getProviders = () => async (dispatch, getState) => {
    try {
        const { userSelectedService } = getState().service
        const url = `/provider/provider_list_type_subtype?sub_type=${userSelectedService}`
        const response = await axios.get(url)
        console.log(response)
    } catch (err) {
        console.log('Error in getting providers', err)
    }
}

export const createRequest = (region, priceType, socket, userLocation) => async (dispatch, getState) => {

          
           console.log('region',region)
           console.log('socket',socket)
            console.log('userLocation',userLocation)
    try {
        const { userSelectedService } = getState().service

          console.log("userSelectedService", userSelectedService);

        const user = getState().auth.user
        const { token, id } = user
        const body = {
            latitude: region.latitude,
            longitude: region.longitude,
            id: id,
            token: token,
            distance: 1,
            type: userSelectedService,
            payment_opt: priceType,
            card_id: 1
        }
        const response = await axios.post('/user/createrequest', body)
        console.log(body)
        console.log('Got a Response for Request', response.data)
        if (response.data.success) {
            socket.emit('join', response.data.walker.id)
            firebase.database().ref(`requests/${response.data.walker.id}/${response.data.request_id}`).set({
                request_id: response.data.request_id,
                walker: response.data.walker,
                owner: user,
                timeOut: new Date().setTime(Date.now() + (REQUEST_TIME_OUT_SEC * 1000)),
                paymentType: priceType,
                destination: userLocation,
                isRejected: false
            })
            firebase.database().ref(`pendingRequests/${id}`).set({
                request_id: response.data.request_id,
                walkerId: response.data.walker.id
            })
            dispatch({
                type: REQUEST_SEND,
                walker: response.data.walker,
                requestId: response.data.request_id
            })
        } else {
            dispatch({
                type: REQUEST_PROVIDER_NOT_FOUND
            })
        }
    } catch (err) {
        console.log('Error', err.message)
    }
}

export const createRequestResere = (region, priceType, socket, userLocation, id, token, user ) => async (dispatch, getState) => {

            console.log('region',region)
            console.log('socket',socket)
            console.log('user',user)
    try {
        // const { userSelectedService } = getState().service
          const wakeruser = getState().auth.user


            console.log("wakeruser", wakeruser)

          console.log("wakeruser", wakeruser.id)
        const walkerid  = wakeruser.id

        console.log("walkerid", walkerid )
        const body = {
            latitude: region.latitude,
            longitude: region.longitude,
            id: id,
            token: token,
            distance: 1,
            type: user.serviceid,
            payment_opt: priceType,
            card_id: 1
        }
         console.log(body)
        const response = await axios.post('/user/createrequest', body)
       
        console.log('Got a Response for Request  xx ', response.data)
     
        const resultupdate = await axios.post('/user/resevationupdate', {request_id : response.data.request_id , res_id : user.res_id })

        if (response.data.success) {

            console.log('step 1')
            socket.emit('join', walkerid)
            firebase.database().ref(`requests/${walkerid}/${response.data.request_id}`).set({
                request_id: response.data.request_id,
                walker: wakeruser,
                owner: user,
                timeOut: new Date().setTime(Date.now() + (REQUEST_TIME_OUT_SEC * 1000)),
                paymentType: priceType,
                destination: userLocation
            })
            console.log('step 2')
            firebase.database().ref(`pendingRequests/${id}`).set({
                request_id: response.data.request_id,
                walkerId: walkerid
            })
            console.log('step 3')
            dispatch({
                type: REQUEST_SEND,
                walker: wakeruser,
                requestId: response.data.request_id
            })
            console.log('step 4')
        } else {
            console.log('step 5')
            dispatch({
                type: REQUEST_PROVIDER_NOT_FOUND
            })
            console.log('step 6')
        }
    } catch (err) {
        console.log('step 7')
        console.log('Error', err.message)
    }
}

export const listenForRequestProposal = () => async (dispatch, getState) => {
    try {
        const { token, id } = getState().auth.user
        const requestId = getState().service.requestId
        const url = `/user/getrequest?id=${id}&token=${token}&request_id=${requestId}`
        const looper = async () => {
            try {
                console.log("I'm inside interval")
                console.log("Body", {token, id, requestId})
                const response = await axios.get(url)
                const data = response.data
                console.log('Service Details', data)
                if (data.confirmed_walker > 0) {
                    dispatch({
                        type: REQUEST_ACCEPT,
                        serviceStatus: 1,
                        walker: data.walker
                    })
                    if (data.is_walk_started > 0) {
                        // dispatch({
                        //     type: REQUEST_PRICE,
                        //     price: data.charge_details.base_price,
                        //     serviceStatus: 2
                        // })
                        dispatch({
                            type: UPDATE_WALKER_CORDS,
                            walker: data.walker
                        })

                        if (data.is_completed > 0) {
                            dispatch({
                                type: REQUEST_COMPLETED
                            })
                        }
                    } else if (data.is_cancelled > 0) {
                        dispatch({
                            type: REQUEST_DECLINE
                        })
                    }
                } else if (data.is_cancelled > 0) {
                    dispatch({
                        type: REQUEST_DECLINE
                    })
                }
            } catch (err) {
                console.log('Error in listening for request status', err)
            }
        }
        looper()
        // const intervalId = setInterval(looper, 10000)
    } catch (err) {
        alert(err.message)
    }
}

export const checkForPendingRequests = () => async (dispatch, getState) => {
    try {
        const { id, token } = getState().auth.user
        const userType = getState().auth.userType
        const url = `/${userType}/requestinprogress?id=${id}&token=${token}`
        const resposne = await axios.get(url)

        if (resposne.data.success && resposne.data.request_id > 0) {
            console.log('Pending request', resposne.data)
            dispatch({
                type: UPDATE_REQUEST_ID,
                requestId: resposne.data.request_id,
            })
        } else {
            console.log('No Pending requests', resposne.data)
            dispatch(listenForRequest())
        }
    } catch (err) {
        console.log('Error in checkPendingRequests', err)
    }
}

export const initPendingRequest = (requestId, walker) => ({
    type: REQUEST_SEND,
    walker,
    requestId
})

export const clearPendingRequest = () => ({
    type: CLEAR_PENDING_REQUEST
})

export const initPendingRequestProvider = () => async (dispatch, getState) => {
    try {
        let url = '/provider/getrequest'
        const { id, token } = getState().auth.user
        const request_id = getState().service.requestId
        url += `?id=${id}&token=${token}&request_id=${request_id}`
        const response = await axios.get(url)
        console.log('Pending Request', response.data)
        if (response.data.success) {
            delete Object.assign(response.data, { ['request_data']: response.data['request'] })['request']
            dispatch({
                type: PENDING_REQUEST_PROVIDER,
                payload: response.data,
                requestId: request_id
            })
            dispatch(updateProviderLocation())
        } else {
            dispatch({
                type: NO_REQUEST_RECEIVED
            })
        }
    } catch (err) {
        console.log('Error in init pending request')
    }
}

export const listenForRequest = () => async (dispatch, getState) => {
    try {
        const { id, token } = getState().auth.user
        let url = `/provider/getrequests?id=${id}&token=${token}`
        const response = await axios.get(url)
        if (response.data.incoming_requests.length > 0) {
            dispatch({
                type: REQUEST_RECEIVED,
                payload: response.data.incoming_requests[0]
            })
        } else {
            dispatch({
                type: NO_REQUEST_RECEIVED
            })
        }
        console.log('listening for request', response.data.incoming_requests)
    } catch (err) {
        console.log('Error in getting request', err)
    }
}

export const acceptRequest = (requestId) => async (dispatch, getState) => {
    try {
        const { id, token } = getState().auth.user
        const url = `/provider/respondrequest?id=${id}&token=${token}&request_id=${requestId}&accepted=1&reason=`
        const response = await axios.post(url)
        console.log('Request Accepted', response.data)
    } catch (err) {
        console.log('Accept Request Failed', err.message)
    }
}

export const declineRequest = (requestId) => async (dispatch, getState) => {
    try {
        const { id, token } = getState().auth.user
        const url = `/provider/respondrequest?id=${id}&token=${token}&request_id=${requestId}&accepted=0&reason=`
        const response = await axios.post(url)
        console.log('Decline Request', response)
    } catch (err) {
        console.log('Error in request accept', err.message)
    }
}

export const updateProviderLocation = () => async (dispatch, getState) => {
    try {
        const { id, token } = getState().auth.user
        const request_id = getState().service.request.request_id
        const { latitude, longitude } = getState().location.region
        // const url = `/request/location?id=${id}&token=${token}&request_id=${request_id}&accepted=0&reason=`
        const body = {
            id,
            token,
            latitude,
            longitude,
            request_id
        }
        const response = await axios.post('/request/location', body)
        if (response.data.success) {
            dispatch({
                type: UPDATE_DISANCE_AND_TIME,
                distance: response.data.distance,
                time: response.data.time
            })
        }
        console.log('Updating Provider Location', response.data)
    } catch (err) {
        console.log('Error in update provider location', err.message)
    }
}

export const rejectRequest = (request_id) => async (dispatch, getState) => {
    try {
        const local = locals[getState().auth.local].toast
        const { id, token } = getState().auth.user
        // const requestId = getState().service.request.request_id
        const response = await axios.post(`/user/cancelrequest?id=${id}&token=${token}&request_id=${request_id}&reason=''`)
        if (response.data.success) {
            ToastAndroid.show(local.requestRejected, ToastAndroid.LONG)
            dispatch({
                type: REQUEST_REJECTED,
                serviceStatus: 0
            })
        }
        console.log('Request Rejected', response.data)
    } catch (err) {
        console.log('Error in rejecting request', err.message) 
    }
}

export const sendReview = (rating, comment, navigatation) => async (dispatch, getState) => {
    try {
        const { id, token } = getState().auth.user
        const request_id = getState().service.request.request_id
        const userType = getState().auth.userType
        const body = {
            id,
            token,
            request_id,
            comment,
            rating
        }
        const response = await axios.post(`/${userType}/rating`, body)
        console.log('review done', response.data)
        if (response.data.success) {
            console.log('review done', response.data)
            dispatch({
                type: RATING_DONE
            })
            ToastAndroid.show('Reting Done', ToastAndroid.LONG)
            navigatation.goBack()
            navigatation.state.params.onRatingDone()
        } else {
            console.log('review failed', response.data)
            ToastAndroid.show('Reting Failed', ToastAndroid.LONG)
            navigatation.state.params.onRatingDone()
        }
    } catch (err) {
        console.log('Error in review', err)
    }
}

export const fetchHistory = () => async (dispatch, getState) => {
    try {
        const { id, token } = getState().auth.user
        const userType = getState().auth.userType
        const url = `/${userType}/history?id=${id}&token=${token}`
        const response = await axios.get(url)
        console.log('History Fetched', response.data)
        if (response.data.success) {
            dispatch({
                type: HISTORY_FETCHED,
                histories: response.data.requests
            })
        }
    } catch (err) {
        console.log('Error in fetching history', err)
    }
}

export const fetchWalkers = (id) => async (dispatch) => {
    try {
        const response = await axios.get(`/provider/provider_list_type_subtype?service_sub_type=${id}`)
        console.log("get Providers List", id, response.data)
        if (response.data.success && response.data.result) {
            dispatch({
                type: UPDATE_WALKERS,
                walkers: response.data.result
            })
        }
    } catch (err) {
        console.log('Error in fetching walker', err)
    }
}

export const fetchWalkersfilter = (id, lat, long) => async (dispatch) => {
 
   //  console.log( "Data", id)
    // console.log( "Data", lat)
    // console.log( "Data", long)

    try {
        const response = await axios.get(`/provider/provider_list_type_subtype_near_me?lat=${lat}&long=${long}&service_sub_type=${id}`)
        console.log("get Providers filter  List", id, response.data)
        if (response.data.success && response.data.result) {
            dispatch({
                type: UPDATE_WALKERS,
                walkers: response.data.result
            })
        }
    } catch (err) {
        console.log('Error in fetching walker', err)
    }
}


export const fetchReservationlist = () => async (dispatch, getState) => {
    try {
         const { id, token } = getState().auth.user

         const userType = getState().auth.userType   

          console.log("userType", userType )

        if(userType == 'user'){
         var  response = await axios.get(`/user/reservationlist/${id}`)
        }else{

        var  response = await axios.get(`/user/reservationlist/get/${id}`)
        }
        console.log("this id my Reservation data", response)
        if (response.data.success) {
            dispatch({
                type: RESERVATIONLIST,
                reserve: response.data.data
            })
        }
    } catch (err) {
        console.log('Error in fetching walker', err)
    }
}

export const selectService = (id) => (dispatch) => {
    dispatch({
        type: SELECT_SERVICE,
        id: id
    })
    dispatch(fetchWalkers(id))
}

export const toggleServiceSelection = id => (dispatch) => {
    dispatch({
        type: TOGGLE_SERVICE_SELECTION,
        id
    })
    dispatch(fetchWalkers(id))
}
