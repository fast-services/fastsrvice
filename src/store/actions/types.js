export const LOGIN_SESSION = 'login_session'
export const LOGIN_USER = 'login_type'
export const LOGIN_SUCCESS = 'login_done'
export const LOGIN_FAILED = 'login_failed'
export const LOG_OUT = 'log_out'
export const UPDATE_SUCCESS = 'update success'
export const UPDATE_FAILED = 'update failed'
export const CLEAR_LOGIN_ERROR = 'clear login error'

export const SET_USER_TYPE = 'user_type'

export const UPDATE_CURRENT_LOCATION = 'update_current_location'

export const UPDATE_DISANCE_AND_TIME = 'update_distance_and_time'

export const UPDATE_LOCALS = 'update_locals'

export const FETCH_SERVICES = 'fetch_services'
export const FETCH_SUB_SERVICES = 'fetch_sub_services'
export const SERVICE_FETCHING_ERROR = 'service_fetching_error'
export const SELECT_SERVICE = 'service_select'
export const TOGGLE_SERVICE_SELECTION = 'toggle_service'

export const PENDING_REQUEST_PROVIDER = 'pending_request_provider'
export const UPDATE_REQUEST_ID = 'update_request_id'
export const CLEAR_PENDING_REQUEST = 'clear_pending_request'

export const REQUEST_SEND = 'request_send'
export const REQUEST_ACCEPT = 'request_accept'
export const REQUEST_DECLINE = 'request_decline'
export const REQUEST_COMPLETED = 'request_completed'
export const REQUEST_PRICE = 'request_price'
export const REQUEST_PROVIDER_NOT_FOUND = 'provider_not_found'
export const UPDATE_WALKER_CORDS = 'update_walker_cords'
export const REQUEST_REJECTED = 'reject_request'

export const REQUEST_RECEIVED = 'request_received'
export const REQUEST_ACCEPTED = 'request_accepted'
export const REQUEST_DECLINED = 'request_declined'
export const NO_REQUEST_RECEIVED = 'no_request_received'
// export const REQUEST_ACCEPTED = 'request_accepted'
// export const REQUEST_DECLINE = 'requst_decline'

export const UPDATE_WALKERS = 'update_walkers'

export const BILL_READY = 'bill ready'

export const HISTORY_FETCHED = 'history_fetched'

export const CREATE_REQUEST = 'create_request'

export const RATING_DONE = 'rating_done'

export const ADD_PRICE = 'add_price'

export const PRICE_ADDED = 'price_added'

export const RESERVATIONLIST = 'reservationlist'