import { Platform, ToastAndroid } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import axios, {setClientToken } from '../../server/apis'
import {
    LOGIN_USER,
    LOGIN_SUCCESS,
    LOGIN_FAILED,
    LOG_OUT,
    LOGIN_SESSION,
    SET_USER_TYPE,
    UPDATE_FAILED,
    CLEAR_LOGIN_ERROR,
    UPDATE_SUCCESS
} from './types'
import { getMainServiceTypes, getAllServices } from '../../utils/utils'
import {locals} from '../../utils/locals'
import Toast from 'react-native-simple-toast'
export const loginUser = (credentials, navigation) => async (dispatch, getState) => {
    console.log(credentials)
    ToastAndroid.show('Please wait....', ToastAndroid.LONG)
    const body = { }

        body.email = credentials.email
        body.password = credentials.password
      //  console.log("response.data.success",body)

    try {
        dispatch({ type: LOGIN_USER })
        const response = await axios.post(`/loginWalker`, body)
      
        if (response.data.success) {
           // console.log("response.data.success",response.data.data.token)
           
            try {
                await AsyncStorage.setItem('token', response.data.data.token)
                await AsyncStorage.setItem('user', JSON.stringify(response.data.data.user_details))
           
            } catch (err) {
                console.log('Error in token store',err)
                ToastAndroid.show("Email or password is wrong.", ToastAndroid.LONG)
            }
            dispatch(loginSuccess(response.data.data.user_details))
            if (response.data.data.user_details.is_active > 0 ) {
                console.log("step 1")
                navigation.navigate('DrawerNavigationRoutes')
                ToastAndroid.show("LOGIN SUCCESSFUL", ToastAndroid.LONG)
                setClientToken(response.data.data.token)
            }else if (response.data.is_active <= 0) {
                console.log("step 2")
                ToastAndroid.show("Your account is not activate", ToastAndroid.LONG)
            }else {
                console.log("step 3")
            }
        } else {
            console.log("step 4")
            ToastAndroid.show("Login Error", ToastAndroid.LONG)
            console.log('Error', response.data.error)
            dispatch(loginFailed(response.data.error))
            setTimeout(() => {
                dispatch({
                    type: CLEAR_LOGIN_ERROR
                })
            }, 3000)
        }

    } catch (err) {
        console.log('Login Failed', err)
        dispatch(loginFailed(err))
        setTimeout(() => {
            dispatch({
                type: CLEAR_LOGIN_ERROR
            })
        }, 3000)
    }
}

export const logOut = () => async (dispatch) => {
    await AsyncStorage.removeItem('user')
    dispatch({
        type: LOG_OUT
    })
}

export const registerUser = (credentials, navigation) => async (dispatch, getState) => {
    console.log('Registering user', credentials)
    ToastAndroid.show('Please wait....', ToastAndroid.LONG)
    const body = {
        phone: credentials.phone,
        email: credentials.email,
        password: credentials.password,
        user_type: 3
    }

    console.log(body)

    try {
         
        const response = await axios.post(`/walkerRegisteration`, body)

        console.log("response", response.data);

           if (response.data.success) {
               console.log('Step 3')
               ToastAndroid.show("Registration Successful", ToastAndroid.LONG)
               navigation.navigate('Login')
            }  else {
             console.log('Error In Registration', response.data.error)
             ToastAndroid.show('This email is already register', ToastAndroid.LONG)
             navigation.navigate('RegisterScreen')
             
          }

    } catch (err) {
        console.log('Registartion Failed e4', err)
        ToastAndroid.show('something is wrong with me', ToastAndroid.LONG)
        navigation.navigate('RegisterScreen')
    }
}

export const updatePicture = (data) => async (dispatch, getState)=>{

    //console.log("updade File data",data)
         
     let  userdata =  await AsyncStorage.getItem('user')
    let token  = await AsyncStorage.getItem('token')

       console.log(userdata)

       var obj = JSON.parse(userdata);

    let body = {
        user_id : obj.id,
        image : data
    }

     // console.log("body",body)

        const response = await axios.post(`/updateProfileImage`, body)
          //console.log("response",response)

        if (response.data.success) {
          //  console.log('Update Success', response)
            (local.profileUpdated, ToastAndroid.LONG)
        } else {
            console.log('Error In Update Profile Picture ', response.data.error)
            ToastAndroid.show(local.profileUpdateFailed, ToastAndroid.LONG)
        }

}

export const updateProfile = (credentials, navigation) => async (dispatch, getState) => {
    console.log('Update Profile', credentials)
    const local = locals[getState().auth.local].toast
    const userType = getState().auth.userType
    const { id, token } = getState().auth.user
    const body = {
        id, token,
        first_name: credentials.firstName,
        last_name: credentials.lastName,
        email: credentials.email,
        address: credentials.address,
        phone: credentials.number,
        bio: credentials.bio,
        zipcode: '',
        country: '',
        state: credentials.profileImage
    }

    if (credentials.old_password !== '') {
        body.new_password = credentials.new_password
        body.old_password = credentials.old_password
    }

    if (userType === 'provider') {
        body.maintype = getMainServiceTypes(credentials.services).join(',')
        body.type = getAllServices(credentials.services).join(',')
    }

    console.log(body)

    try {
        dispatch({ type: LOGIN_USER })
        const response = await axios.post(`/${getState().auth.userType}/update`, body)
        console.log("response",response.data)
        if (response.data.success) {
            console.log('Update Success', response)
            try {
                await AsyncStorage.setItem('token', response.data.token)
                await AsyncStorage.setItem('user', JSON.stringify(response.data))
            } catch (err) {
                console.log('Error in token store')
            }
            dispatch(loginSuccess(response.data))
            dispatch(updateSuccess())
            setTimeout(() => {
                dispatch({
                    type: CLEAR_LOGIN_ERROR
                })
            }, 5000)
            ToastAndroid.show(local.profileUpdated, ToastAndroid.LONG)
        } else {
            console.log('Error In Update Profile', response.data.error)
            ToastAndroid.show(local.profileUpdateFailed, ToastAndroid.LONG)
            dispatch(updateFailed())
            setTimeout(() => {
                dispatch({
                    type: CLEAR_LOGIN_ERROR
                })
            }, 5000)
        }

    } catch (err) {
        console.log('Registartion Failed', err)
        dispatch(loginFailed(err))
        dispatch(updateFailed())
        setTimeout(() => {
            dispatch({
                type: CLEAR_LOGIN_ERROR
            })
        }, 5000)
    }
}

export const restoreLoginSession = () => async (dispatch) => {
    console.log("i'm running here")
    let user = await AsyncStorage.getItem('user')
    user = JSON.parse(user)
    const userType = await AsyncStorage.getItem('UserType')
    dispatch({
        type: LOGIN_SESSION,
        user: user,
        userType: userType
    })
}

const loginFailed = (error) => ({
    type: LOGIN_FAILED,
    error: error
})

const loginSuccess = (user, msg) => ({
    type: LOGIN_SUCCESS,
    user: user,
    msg
})

const updateSuccess = () => ({
    type: UPDATE_SUCCESS
})

const updateFailed = () => ({
    type: UPDATE_FAILED
})

export const setUserType = (userType, nav) => async (dispatch) => {
    await AsyncStorage.setItem('UserType', userType)
    dispatch({
        type: SET_USER_TYPE,
        userType: userType
    })
    nav.navigate('Login')
}

export const reservation = (reservation_data) => async (dispatch) => {

    console.log(reservation_data);

     const rdata = {
         user_id: reservation_data.userid,
         provider_id: reservation_data.selected_provider,
         reservedate: reservation_data.datetime,
         paymethod: reservation_data.paymenType,
         amount: reservation_data.budget,
         service: reservation_data.service,
         serviceid: reservation_data.serviceid
    }

     const response = await axios.post(`/user/reservation`,rdata);
      
     if(response.data.success){
       
            Toast.show("Reservation Completed", Toast.LONG, Toast.TOP)
      
     }else{

          Toast.show("Reservation Failed", Toast.LONG, Toast.TOP)
       }    
}

 export const cancelrequest = (data) => async (dispatch, getState) => {

    const { id, token } = getState().auth.user

     const rdata = {
         cancelid: data.cancelid,
    }

     const response = await axios.post(`/user/reservationlist/cancel`,rdata)

     console.log(response)
      
     if(response.data.success){
       
            Toast.show("Reservation Cancel ", Toast.LONG, Toast.TOP)
      
     }else{

          Toast.show("Reservation Cancel Failed", Toast.LONG, Toast.TOP)
       }   
}

export const acceptrequest = (data) => async (dispatch, getState) => {

    const { id, token } = getState().auth.user

    console.log("acceptid id is =", data.acceptid);

     const rdata = {
         acceptid: data.acceptid,
    }

     const response = await axios.post(`/user/reservationlist/accept`,rdata)

     console.log(response)
      
     if(response.data.success){
       
            Toast.show("Reservation Accept ", Toast.LONG, Toast.TOP)
      
     }else{

          Toast.show("Reservation Accept Failed", Toast.LONG, Toast.TOP)
       }   
}