
export default {
    auth: {
        login: 'Login',
        email: 'Email',
        password: 'Password',
        oldPassword: 'Old password',
        newPassword: 'New password',
        forgotPassword: 'Forgot my password',
        notMember: 'Not a member?',
        register: 'Register',
        facebook: 'Continue with facebook',
        firstName: 'First Name',
        lastName: 'Last Name',
        number: 'Enter your contact no.',
        bio: 'Describe yourself',
        address: 'Enter your address',
        rePassword: 'Re-enter password',
        providerType: 'Provider Type',
        individual: 'Individual',
        business: 'Bussiness',
        subscriptionType: 'Subscription Type',
        premium: 'Premium',
        normal: 'Normal',
        selectServiceType: 'Select the type of service',
        editProfile: 'Edit Profile',
        updateProfile: 'Update Profile',

        loginFailed: 'Login Failed',
        registartionFailed: 'Registration Faield',
        emailError: 'This email address is already registered',
        phoneError: 'This contact number is already registered', 
        updateSuccess: 'Profile Updated',
        updateFailed: 'Profile Update Failed',

        errorFirstName: 'Please Enter Right Name',
        errorLastName: 'Last name should not be empty',
        errorEmail: 'Email is not valid',
        errorNumber: 'Phone Number is not valid',
        errorBio: 'Please put something that describe you',
        errorAddress: 'Address should not be empty',
        errorPassword: 'Password should be 6 characters long',
        errorRePassword: 'Password did\'t match',
        errorOldPassword: 'Password not currect',
        errorProviderType: 'Please Choose a Provider Type',
        errorSubscriptionType: 'Please choose Subscription type',
        errorService: 'Please choose atleast one service',
       



    },
    requestStatus: {
        status: 'Status',
        accept: 'Provider accepted the request',
        coming: 'Provider coming at your location',
        arrived: 'Provider have arrived at your location',
        started: 'Work has been started',
        accomplish: 'Work has been accomplished'
    },
    navDrawer: {
        editProfile: 'Edit Profile',
        home: 'Home',
        chats: 'Chats',
        history: 'History',
        settings: 'Settings',
        help: 'Help',
        logout: 'Logout',
        professionals: 'Professionals',
        upgrade: 'Upgrade to pro',
        reserve: 'Request'

    },
    userHome: {
        bookNow: 'Book Now',
        cash: 'Cash',
        card: 'Credit/Debit Cart',
        paypal: 'Paypal',
        done: 'Done',
        promocode: 'Enter promocode',
        choosePaymentMethod: 'Choose Payment Method',
        selectPayment: 'Select Payment Type',
        noProvider: 'No Provider found in this region',
        contactingProvider: 'Contacting Service Provider',
        reserve : 'Reserve',
        budgetPlaceholder: 'Enter Your Budget',
        selectdate: "Select Date",
        selecttime: "Select Time",
        nearby : "Nearby",
        professionals: 'Professionals', 
        langs: 'en-EN'

    },
    providerHome: {
        workDone: 'Work Done',
        workStart: 'Start Work',
        reached: 'I\'m Reached',
        secondLeft: 'Seconds left',
        accept: 'Accept',
        decline: 'Decline',
        reject: 'Reject',
        destination: 'Destination',
        title : "New Request",
        message : "Hey, You have a new request please check it."
    },
    priceDialog: {
        enterPrice: 'Enter Price that you want to charge',
        confirm: 'Confirm Request'
    },
    priceAcceptReject: {
        accept: 'Accept',
        reject: 'Reject',
        serviceCharge: 'Service Charges'
    },
    chat: {
        typeMessage: 'Type a message',
        chatHistory: 'Chat History',
        typing: 'Typing...',
        close: 'Close'
    },
    entryRoute: {
        selectLanguage: 'Select your language',
        imUser: 'I\'m a User',
        imProvider: 'I\'m a Service Provider'
    },
    review: {
        review: 'Review',
        rate: 'Rate',
        done: 'Rating Done',
        failed: 'Rating Failed',
        now: 'Rate Now',
        feedback: 'Feedback',
        doneBtn: 'Done'
    },
    invoice: {
        paymentSuccess: 'Payment Success',
        paymentFailed: 'Payment Failed',
        rating : 'Rating',
        areYouWannaRating: 'are you wanna give rating',
        no: 'No',
        rate: 'Rate',
        doNotClose: 'Please do not close the app and go back',
        invoice: 'Invoice',
        serviceCharge: 'Service Charges',
        basePrice: 'Base Price',
        additionCharge: 'Additional Charges',
        promocode: 'Promocode',
        total : 'Total',
        continue: 'Continue',
        cancel: 'Cancel'
    },
    upgrade: {
        premium: 'Premium',
        experince: 'Experience the better support',
        features: [
            'Get more requests',
            'No Ads'
        ],
        upgrade: 'Upgrade',
        noThanks: 'No Thanks'
    },
    toast: {
        requestAccepted: 'Request Accepted',
        requestRejected: 'Request Rejected',
        workCompleted: 'Work is completed',
        gotRequest: 'Got a request',
        enableToSendRequest: 'Enable to send request',
        paymentSuccess: 'Payment Success',
        paymentFailed: 'Payment Failed',
        registartionSuccess: 'Thank You for registration, Now wait for admin aproval',
        registartionFailed: 'Registartion Failed',
        loginSuccess: 'Login Success',
        loginFailed: 'Login Failed',
        adminNotApproved: 'Admin not aproved you yet, Please wait for some time',
        profileUpdated: 'Profile Updated',
        profileUpdateFailed: 'Enable to update profile',
        locationUpdated: 'Location updated',
        locatonUpdatateFailed: 'Unable to update your Location',
        resevationSucessfull: 'Resevation Completed',
        resevationfailed: 'Resevation Failed'
    },
    resevations:{
       accepted:'Accepted',
       panding: 'Pending',
       canceled: 'Canceled',
       completed: 'Completed',
       requestAccepted:'Request Accepted',
       requestcancel: 'Request Canceled',
       requestpanding: 'Request Panding',
       requestCompleted: 'Request Completed',
       service: 'Service',
       budget :'Budget',
       paymenttype: 'Payment Type',
       date: 'Date',
       accepttext:'Accept',
       canceltext: 'Cancel',
       startwork: 'Start Work',
       request: 'Request'
    }
}
