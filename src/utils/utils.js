import _ from 'underscore'
import deepClone from 'lodash/cloneDeep'

/**
 * @param {Array}  arr - array where to search
 * @param {any} val - value for search in array
 * @returns {boolean}
 */
export const isInArray = (arr, val) => arr.findIndex((_, i) => _ === val) !== -1

/**
 * This method clone object deeply and return new instance or brand new object with new reference
 * @param {Object} obj - Object to deeply clone
 * @returns {Object}
 */
export const deepCloneState = (obj) => deepClone(obj)

/**
 * This method formate the timestamp into local string
 * @param {number} timeStamp
 * @returns {string}
 */
export const formateDate = (timeStamp) => {
    const date = new Date(timeStamp)
    // const formateDate = `${date.getHours()}:${date.getMinutes}`
    return date.toLocaleTimeString([], {
        hour: '2-digit',
        minute: '2-digit'
    })
}

/**
 * @param {Object} obj
 * @returns {boolean}
 */
export const isObjectEmpty = (obj) => _.isEmpty(obj)

/**
 * 
 * @param {Set} set1 set1 compare to set2
 * @param {Set} set2 set2 compare to set1
 * @returns {Boolean} if both are equal
 */
export const isSetsEqual = (set1, set2) => {
    if (set1.size !== set2.size) return false
    for (let i of set1) if (!set2.has(i)) return false
    return true
}

/**
 * as a name suggest this method checks the email is valid or not
 * @param {string} email 
 * @returns {boolean}
 */
export const isEmailValid = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(String(email).toLowerCase())
}

/**
 * this method checks the request is expired or not
 * @param {Object} request 
 * @returns {boolean}
 */
export const isRequestExpired = (request) => request.timeOut < Date.now()


/**
 * this method returns remaining time of unAccepted request
 * @param {Object} request 
 * @returns {number} - number of seconds are remaining to expire request
 */
export const getRequestRemainingTime = (request) => Math.floor(((request.timeOut - Date.now()) / 1000))

/**
 * 
 * @param {Object} services in form of { mainServiceType: [serviceTypes] }
 * @returns {Array} return array with the main services types
 */
export const getMainServiceTypes = (services) => Object.keys(services)

/**
 * 
 * @param {Object} services  services in form of { mainServiceType: [serviceTypes] }
 * @returns {Array} return array of all sub services
 */
export const getAllServices = (services) => {
    const data = []
    for (service in services) {
        for (s of services[service]) {
            data.push(s)
        }
    }
    return data
}

/**
 * 
 * @param {Array} mainTypes array of main types with only id's
 * @param {Array} subTypes array of sub types with only id's
 * @returns {Object} object in form of { mainServiceType: [serviceTypes] }
 */
export const composeServices = (mainTypes, subTypes) => {
    const services = {}
    for(let i=0; i<mainTypes.length; i++) {
        if (services[mainTypes[i]] == undefined) { services[mainTypes[i]] = [] }
        services[mainTypes[i]].push(subTypes[i])
    }
    return services
}
