/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {View, Text} from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import 'react-native-gesture-handler';
// import { NavigationContainer } from '@react-navigation/native';
// import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';

import RegisterScreen from './screens/registerScreen'
import SplashScreen from './screens/splashScreen'
import UserType from './screens/userType'
import passwordScreen from './screens/passwordScreen'
import VerificationScreen from './screens/verificationScreen'
import LoginScreen from './screens/loginScreen'
import SendCode from './screens/sendCodeScreen'
import recoveryCode from './screens/recoveryCode'
import DrawerNavigationRoutes from './screens/DrawerNavigationRoutes';


const Auth = createStackNavigator({
  //Stack Navigator for Login and Sign up Screen
 
  RegisterScreen: {
    screen: RegisterScreen,
    navigationOptions: {
      title: 'Register',
      headerShown: false,
      headerStyle: {
        backgroundColor: '#fff',
      },
      headerTintColor: '#4362bf',
    },
  },
  Password: {
    screen: passwordScreen,
    navigationOptions: {
      title: 'Password ',
      headerStyle: {
        backgroundColor: '#fff',
      },
      headerTintColor: '#4362bf',
    },
  },
  Login: {
    screen: LoginScreen,
    navigationOptions: {
      title: 'Login',
      headerStyle: {
        backgroundColor: '#fff',
      },
      headerTintColor: '#4362bf',
    },
  },
  SendCode: {
    screen: SendCode,
    navigationOptions: {
      title: 'Send Code',
      headerStyle: {
        backgroundColor: '#fff',
      },
      headerTintColor: '#4362bf',
    },
  },
  recoveryCode: {
    screen: recoveryCode,
    navigationOptions: {
      title: 'Recovery Code',
      headerStyle: {
        backgroundColor: '#fff',
      },
      headerTintColor: '#4362bf',
    },
  }
  
});

/* Switch Navigator for those screens which needs to be switched only once
  and we don't want to switch back once we switch from them to the next one */
const App = createSwitchNavigator({ 
  SplashScreen: {
    /* SplashScreen which will come once for 5 Seconds */
    screen: SplashScreen,
    navigationOptions: {
      /* Hiding header for Splash Screen */
      headerShown: false,
    },
  },
  Auth: {
    /* Auth Navigator which includer Login Signup will come once */
    screen: Auth,
  },
  DrawerNavigationRoutes: {
    /* Navigation Drawer as a landing page */
    screen: DrawerNavigationRoutes,
    navigationOptions: {
      /* Hiding header for Navigation Drawer as we will use our custom header */
      headerShown: false,
    },
  },
  
});

export default createAppContainer(App);

// const Stack = createStackNavigator();

// function App() {
//   return (
//     <NavigationContainer>
//       <Stack.Navigator>
//         <Stack.Screen 
//             name="splash"
//             component={SplashScreen}
//             options={{ headerShown: false }} />
//         <Stack.Screen name="Verification" component={VerificationScreen} options={{ title: 'Home' }} />
//         <Stack.Screen name="Home" component={HomeScreen} options={{ title: 'Home' }} />
//         <Stack.Screen name="Register" component={RegisterScreen} options={{ title: 'Registration',headerShown: false }} />
//         <Stack.Screen name="Password" component={passwordScreen} options={{ title: 'Password' }} />
//         <Stack.Screen name="UserType" component={UserType} options={{ title: 'User Type', headerShown: false  }} />
//         <Stack.Screen name="Login" component={LoginScreen} options={{ title: 'Login' }} />
//         <Stack.Screen name="SendCode" component={SendCode} options={{ title: 'Send Code' }} />
//         <Stack.Screen name="recoveryCode" component={recoveryCode} options={{ title: 'Recovery Code' }} />
       
//       </Stack.Navigator>
//     </NavigationContainer>
//   );
// }


// export default App;
