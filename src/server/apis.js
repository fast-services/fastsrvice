import axios from 'axios';

// Create axios client, pre-configured with baseURL
let Axios = axios.create({
  baseURL: 'https://appsontechnologies.in/fastservice/public/api',
  timeout: 10000,
});

// Set JSON Web Token in Client to be included in all calls
export const setClientToken = token => {
    Axios.interceptors.request.use(function(config) {
    config.headers.Authorization = `Bearer ${token}`;
    return config;
  });
};

export default Axios;