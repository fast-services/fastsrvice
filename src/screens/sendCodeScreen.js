import React from 'react'
import {
    View,
    Text,
    Modal,
    FlatList,
    StyleSheet,
    SafeAreaView,
    Keyboard,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Image
} from 'react-native'
import { text, buttons, styles } from '../components/styles/styles';
import Icon from 'react-native-vector-icons/FontAwesome';
// native base imports
import {
    Container,
    Item,
    ListItem, Body, Input, Label, Radio, Right, Left, Content
} from 'native-base'


class SendCode extends React.Component {

    state = {
        phone : true,
        email : false
    }

    changeMedia(media){

        if (media == 'phone') {
            this.setState({
                phone : true,
                email : false
            })
        } else {
            this.setState({
                phone : false,
                email : true
            })
        } 

    }
    recoverycode(){
        this.props.navigation.navigate('recoveryCode')
    }

    render() {
        return (
            <View style={styles.mainContainer}>

                <View style={styles.centerContainer}>
                    <Text style={[text.heading, text.bold]}> WE HELP YOU ! </Text>
                </View>

                <View style={styles.centerContainer}>
                    <Text style={text.title}> WE WILL RECOVER YOUR PASSWORD  </Text>
                </View>

                <View style={styles.centerContainer}>
                    <Text>
                        We will send a recovery code to one of your registered media choose the channel where you would like to receive the code.
                                        </Text>
                </View>
                <ListItem selected={this.state.email} onPress={()=>this.changeMedia('email')}>
                    <Left>
                        <Text>home*************@gmail.com</Text>
                    </Left>
                </ListItem>
             
                <TouchableOpacity
                    style={[buttons.primary, buttons.round]}
                    activeOpacity={.5}
                    onPress={()=>this.recoverycode()}
                >
                    <Text style={buttons.TextStyle}> SEND CODE {this.state.value}</Text>

                </TouchableOpacity>

            </View>

        )
    }
}

export default SendCode;