import React from 'react'
import {
    View,
    Text,
    Modal,
    FlatList,
    StyleSheet,
    SafeAreaView,
    Keyboard,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Image
} from 'react-native'
import { text, buttons, styles } from '../components/styles/styles';
import Icon from 'react-native-vector-icons/FontAwesome';
// native base imports
import {
    Container,
    Item,
    Input,
    ListItem, Body
} from 'native-base'
import { CheckBox } from 'react-native-elements'
import data from '../../assets/Countries'

// Default render of country flag
const defaultFlag = data.filter(
    obj => obj.name === 'United Kingdom'
)[0].flag

class Verification extends React.Component {


    state = {
        flag: defaultFlag,
        modalVisible: false,
        phoneNumber: '',
        termsAndConditions :true
    }
    onChangeText(key, value) {
        this.setState({
            [key]: value
        })
    }
    showModal() {
        this.setState({ modalVisible: true })
    }
    hideModal() {
        this.setState({ modalVisible: false })
        // Refocus on the Input field after selecting the country code
        this.refs.PhoneInput._root.focus()
    }

    async getCountry(country) {
        const countryData = await data
        try {
            const countryCode = await countryData.filter(
                obj => obj.name === country
            )[0].dial_code
            const countryFlag = await countryData.filter(
                obj => obj.name === country
            )[0].flag
            // Set data from user choice of country
            this.setState({ phoneNumber: countryCode, flag: countryFlag })
            await this.hideModal()
        }
        catch (err) {
            console.log(err)
        }
    }

    render() {
        let { flag } = this.state
        const countryData = data
        return (
            <SafeAreaView style={styles.container}>
                <KeyboardAvoidingView style={styles.container} behavior='padding' enabled>
                    <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>

                        <View style={styles.container}>
                            <Container style={styles.containerVC}>
                                <View style={styles.centerContainer}>
                                    <Text style={text.title}>WELCOME TO THE <Text style={text.bold}> FAST SERVICES ! </Text> </Text>
                                </View>
                                <View style={styles.logoContainer}>
                                    <Image
                                        style={{
                                            width: '50%',
                                            resizeMode: 'contain'
                                        }}
                                        source={require('../../assets/images/logo.png')} />
                                </View>
                                {/* Phone input with native-base */}
                                {/* phone section  */}
                                <Item rounded style={styles.itemStyle}>

                                    {/* country flag */}
                                    <View><Text style={{ fontSize: 25, marginLeft: 10 }} onPress={() => this.showModal()} >{flag}</Text></View>
                                    {/* open modal */}
                                    <Icon
                                        active
                                        name='angle-down'
                                        style={[styles.iconStyle, { marginLeft: 5 }]}
                                        onPress={() => this.showModal()}
                                    />
                                    <Input
                                        style={styles.input}
                                        placeholder='+44766554433'
                                        placeholderTextColor='#adb4bc'
                                        keyboardType={'phone-pad'}
                                        returnKeyType='done'
                                        autoCapitalize='none'
                                        autoCorrect={false}
                                        secureTextEntry={false}
                                        ref='PhoneInput'
                                        value={this.state.phoneNumber}
                                        onChangeText={(val) => {
                                            if (this.state.phoneNumber === '') {
                                                // render UK phone code by default when Modal is not open
                                                this.onChangeText('phoneNumber', defaultCode + val)
                                            } else {
                                                // render country code based on users choice with Modal
                                                this.onChangeText('phoneNumber', val)
                                            }
                                        }
                                        }
                                    />
                                    {/* Modal for country code and flag */}
                                    <Modal
                                        animationType="slide" // fade
                                        transparent={false}
                                        visible={this.state.modalVisible}>
                                        <View style={{ flex: 1 }}>
                                            <View style={{ flex: 10, paddingTop: 30, backgroundColor: '#fff' }}>
                                                <FlatList
                                                    data={countryData}
                                                    keyExtractor={(item, index) => index.toString()}
                                                    renderItem={
                                                        ({ item }) =>
                                                            <TouchableWithoutFeedback
                                                                onPress={() => this.getCountry(item.name)}>
                                                                <View
                                                                    style={
                                                                        [
                                                                            styles.countryStyle,
                                                                            {
                                                                                flexDirection: 'row',
                                                                                alignItems: 'center',
                                                                                justifyContent: 'space-between'
                                                                            }
                                                                        ]
                                                                    }>
                                                                    <Text style={{ fontSize: 30 }}>
                                                                        {item.flag}
                                                                    </Text>
                                                                    <Text style={{ fontSize: 15, color: '#4362bf' }}>
                                                                        {item.name} ({item.dial_code})
                                    </Text>
                                                                </View>
                                                            </TouchableWithoutFeedback>
                                                    }
                                                />
                                            </View>

                                            <TouchableOpacity
                                                onPress={() => this.hideModal()}
                                                style={styles.closeButtonStyle}>
                                                <Text style={styles.textStyle}>
                                                    Close
                          </Text>
                                            </TouchableOpacity>
                                        </View>
                                    </Modal>
                                </Item>
                                <CheckBox
                                    title='ACCEPT THE TERMS AND CONDITIONS'
                                    checked={this.state.termsAndConditions}
                                />
                                <TouchableOpacity
                                    style={[buttons.primary, buttons.round]}
                                    activeOpacity={.5}
                                    onPress={this.updateInfo}
                                >
                                    <Text style={buttons.TextStyle}> REGISTER {this.state.value}</Text>

                                </TouchableOpacity>
                                <View style={styles.centerBox}>
                                    <Text style={text.lightGray}> OR </Text>
                                </View>
                                <TouchableOpacity
                                    style={[buttons.facebook, buttons.round]}
                                    activeOpacity={.5}
                                >
                                    <Text style={buttons.TextStylePrimary}> <Icon name="facebook" size={25} /> </Text>
                                    <Text style={buttons.TextStylePrimary}>  CONTINUE WITH FACEBOOK </Text>

                                </TouchableOpacity>
                            </Container>
                        </View>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

export default Verification;