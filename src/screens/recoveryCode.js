import React from 'react'
import {
    View,
    Text,
    Modal,
    FlatList,
    StyleSheet,
    SafeAreaView,
    Keyboard,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Image
} from 'react-native'
import { text, buttons, styles } from '../components/styles/styles';
import Icon from 'react-native-vector-icons/FontAwesome';
// native base imports
import {
    Container,
    Item,
    ListItem, Body, Input, Label, Radio, Right, Left, Content
} from 'native-base'
import { CheckBox } from 'react-native-elements'
import OTPInputView from '@twotalltotems/react-native-otp-input'
class RecoveryCode extends React.Component {


    state = {
        hidePassword: true,
        icon: 'eye-slash',
        phone: true,
        email: false
    }

    show() {
        this.setState({
            hidePassword: !this.state.hidePassword
        }, () => {
            if (this.state.hidePassword) {
                this.setState({
                    icon: 'eye-slash'
                })
            } else {
                this.setState({
                    icon: 'eye'
                })
            }
        })
    }

    changeMedia(media) {

        if (media == 'phone') {
            this.setState({
                phone: true,
                email: false
            })
        } else {
            this.setState({
                phone: false,
                email: true
            })
        }

    }

    render() {
        return (
            <View style={styles.mainContainer}>

                <View style={styles.centerContainer}>
                    <Text style={[text.heading, text.bold]}>ENTER RECOVERY CODE </Text>
                </View>

                <View style={styles.centerContainer}>
                    <Text>
                        Enter the recovery code we have send to your mail home********@gmail.com
                                        </Text>
                </View>
                <View style={styles.centerContainer}>
                <OTPInputView
    style={{width: '90%', height: 100}}
    pinCount={6}
    // code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
    // onCodeChanged = {code => { this.setState({code})}}
    autoFocusOnLoad
    codeInputFieldStyle={styles.underlineStyleBase}
    codeInputHighlightStyle={styles.underlineStyleHighLighted}
    onCodeFilled = {(code => {
        console.log(`Code is ${code}, you are good to go!`)
    })}
/>  
</View>

                <TouchableOpacity
                    style={[buttons.primary, buttons.round]}
                    activeOpacity={.5}
                    onPress={this.updateInfo}
                >
                    <Text style={buttons.TextStyle}> RESENT </Text>

                </TouchableOpacity>

            </View>

        )
    }
}

export default RecoveryCode;