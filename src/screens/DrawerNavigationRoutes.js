/* This is an Login Registration example from https://aboutreact.com/ */
/* https://aboutreact.com/react-native-login-and-signup/ */

//Import React
import React from 'react';

//Import Navigators
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';

//Import External Screens
import HomeScreen from './drawerScreens/homeScreen';
import SettingsScreen from './drawerScreens/settingScreen';
import ProfileScreen from './drawerScreens/profileScreen';
import CustomSidebarMenu from '../components/CustomSidebarMenu';
import NavigationDrawerHeader from '../components/NavigationDrawerHeader';
import NavigationDrawerHeaderRight from '../components/NavigationDrawerHeaderRight';
import BackButton from '../components/BackButton';
import HistoryScreen from './drawerScreens/historyScreen';
import JobScreen from './drawerScreens/jobsScreen';
import JobViewScreen from './drawerScreens/jonsViewScreen';

const HomeScreen_StackNavigator = createStackNavigator({
  First: {
    screen: HomeScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'REQUEST',
      headerTitleAlign: 'center',
      headerLeft: () => <NavigationDrawerHeader navigationProps={navigation} />,
      headerRight: () => <NavigationDrawerHeaderRight navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#4362bf',
      },
      
      headerTintColor: '#fff',
    }),
  },
});

const SettingsScreen_StackNavigator = createStackNavigator({
  First: {
    screen: SettingsScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Setting',
      headerTitleAlign: 'center',
      headerLeft: () => <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#4362bf',
      },
      headerTintColor: '#fff',
    }),
  },
});

const ProfileScreen_StackNavigator = createStackNavigator({
  First: {
    screen: ProfileScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Profile ',
      headerTitleAlign: 'center',
      headerLeft: () => <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#4362bf',
      },
      headerTintColor: '#fff',
    }),
  },
});


const HistoryScreen_StackNavigator = createStackNavigator({
  First: {
    screen: HistoryScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'History ',
      headerTitleAlign: 'center',
      headerLeft: () => <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#4362bf',
      },
      headerTintColor: '#fff',
    }),
  },
});

const JobScreen_StackNavigator = createStackNavigator({
  First: {
    screen: JobScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'JOBS ',
      headerTitleAlign: 'center',
      headerLeft: () => <NavigationDrawerHeader navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#4362bf',
      },
      headerTintColor: '#fff',
    }),
  },
});

const JobViewScreen_StackNavigator = createStackNavigator({
  First: {
    screen: JobViewScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'VIEW ',
      headerTitleAlign: 'center',
      headerLeft: () => <BackButton navigationProps={navigation} />,
      headerStyle: {
        backgroundColor: '#4362bf',
      },
      headerTintColor: '#fff',
    }),
  },
});

const DrawerNavigatorRoutes = createDrawerNavigator(
  {
    HomeScreen: {
      screen: HomeScreen_StackNavigator,
      navigationOptions: {
        drawerLabel: 'REQUEST',
      }
    },
    SettingsScreen: {
      screen: SettingsScreen_StackNavigator,
      navigationOptions: {
        drawerLabel: 'Setting',
      }
    },
    ProfileScreen: {
      screen: ProfileScreen_StackNavigator,
      navigationOptions: {
        drawerLabel: 'Profile',
      }
    },
    historyScreen: {
      screen: HistoryScreen_StackNavigator,
      navigationOptions: {
        drawerLabel: 'History',
      }
    },
    JobScreen: {
      screen: JobScreen_StackNavigator,
      navigationOptions: {
        drawerLabel: 'JOBS',
      }
    },
    JobViewScreen: {
      screen: JobViewScreen_StackNavigator,
       navigationOptions: {
        drawerLabel: 'VIEW',
      }
    }
  },
  {
    contentComponent: CustomSidebarMenu,
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle',
  }
);
export default DrawerNavigatorRoutes;