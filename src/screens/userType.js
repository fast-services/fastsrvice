import React from 'react';
import { View, Text,Image, ScrollView,TouchableOpacity } from 'react-native';
import {text, buttons, styles } from '../components/styles/styles';
import Icon from 'react-native-vector-icons/FontAwesome';
class UserType extends React.Component {

  selectUserType = (type) => {
    setTimeout(() => {
          this.props.navigation.navigate('Register')
    }, 800)
}

  render() {
    return (
      <View style={styles.containerVC}>
         <View style={styles.centerContainer}>
           <Text style={text.title}>WELCOME TO THE <Text style={ text.bold }> FAST SERVICES ! </Text> </Text>
         </View>
          <View style={styles.logoContainer}>
          <Image
                style={{
                    width: '50%',
                   resizeMode: 'contain'
                }}
                source={require('../../assets/images/logo.png')} />   
            </View>  
         <View>
        <TouchableOpacity
            style={[buttons.primary,buttons.round]}
            activeOpacity = { .5 }
            onPress={()=> this.selectUserType('user')}
            >
              <Text style={buttons.TextStyle}> User </Text>
              
        </TouchableOpacity>   
        <View style={styles.centerBox}>
           <Text style={text.lightGray}> OR </Text>
        </View> 
        <TouchableOpacity
            style={[buttons.primary,buttons.round]}
            activeOpacity = { .5 }
            onPress={()=> this.selectUserType('provider')}
            >
              <Text style={buttons.TextStyle}> Provider </Text>
        </TouchableOpacity>   
      </View> 
      </View>
   
    );
  }
}

export default UserType;