import React from 'react'
import AsyncStorage from '@react-native-community/async-storage'
import {
    View,
    Text,
    Modal,
    FlatList,
    StyleSheet,
    SafeAreaView,
    Keyboard,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Image,
    Platform,
    ScrollView
} from 'react-native'
import { connect } from 'react-redux'
import { text, buttons, styles } from '../components/styles/styles';
import Icon from 'react-native-vector-icons/FontAwesome';
// native base imports
import {
    Container,
    Item,
    ListItem, Body, Input, Label
} from 'native-base'
import { CheckBox } from 'react-native-elements'
import { registerUser } from '../store/actions/index'
class Password extends React.Component {


    state = {
        hidePassword: true,
        icon: 'eye-slash',
        password: '',
        form:{

        }
    }
 
     componentDidMount(){

     // let user  =  AsyncStorage.getItem('registerData')
        this.getRegisterdata()
    }

     getRegisterdata = async () =>{
        let user = await AsyncStorage.getItem('registerData')
        user = JSON.parse(user)
        this.setState({
            form: user
        })

        console.log(user)
     }

    password = (text) => {
        console.log(text);
        this.setState({ password: text })
    }
    show() {
        this.setState({
            hidePassword: !this.state.hidePassword
        }, () => {
            if (this.state.hidePassword) {
                this.setState({
                    icon: 'eye-slash'
                })
            } else {
                this.setState({
                    icon: 'eye'
                })
            }
        })
    }
    login = async () =>{
        let user = await AsyncStorage.getItem('registerData')
        user = JSON.parse(user)
    
         user.password = this.state.password
        
        this.setState({
            form: user
        })
        console.log("formdta",this.state.form)
        this.props.registerUser(this.state.form, this.props.navigation )
       // this.props.navigation.navigate('DrawerNavigationRoutes');
    }
    render() {
        return (
           
            <ScrollView>
              <Container style={styles.containerVC}>
                 <View style={styles.centerContainer}>

                    <Text style={text.title}>WELCOME AGAIN  </Text>
                    <Text style={text.heading}> <Text style={text.bold}> HECTOR MERCADAL </Text> </Text>
                </View>
                <View style={styles.centerContainer}>
                    <Item rounded>
                        <Icon name='lock' size={20} style={styles.inputicon} />
                        <Input placeholder='ENTER PASSWORD'
                            style={styles.passwordFiled}
                            secureTextEntry={this.state.hidePassword}
                            onChangeText={(text) => this.password(text)}
                        />
                        <TouchableOpacity onPress={() => this.show()}>
                            <Icon name={this.state.icon} size={20} style={styles.passwordLock} />
                        </TouchableOpacity>
                    </Item>
                </View>
                {this.state.password == '' ?
                    <TouchableOpacity
                        style={[buttons.primary, buttons.round, styles.disabled]}
                        activeOpacity={.5}
                    
                        disabled
                    >
                        <Text style={buttons.TextStyle}> ENTER {this.state.value}</Text>

                    </TouchableOpacity> :
                    <TouchableOpacity
                        style={[buttons.primary, buttons.round]}
                        activeOpacity={.5}
                        onPress={()=>this.login()}
                    >
                        <Text style={buttons.TextStyle}> ENTER {this.state.value}</Text>

                    </TouchableOpacity>}

                    </Container>
                           
        </ScrollView>
       
        )
    }
}

const mapStateToProps = (state) => {
    return state
}

const mapDispatchToProps = (dispatch) => {
    return {
        registerUser: (cred, nav) => dispatch(registerUser(cred, nav))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Password)