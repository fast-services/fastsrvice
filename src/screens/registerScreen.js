import React from 'react'
import AsyncStorage from '@react-native-community/async-storage'
import {
    View,
    Text,
    Modal,
    FlatList,
    StyleSheet,
    SafeAreaView,
    Keyboard,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Image,
    ScrollView
} from 'react-native'
import { text, buttons, styles } from '../components/styles/styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import Axios, {setClientToken} from '../server/apis'
// native base imports
import {
    Container,
    Item,
    Input,
    ListItem, Body
} from 'native-base'
import { CheckBox } from 'react-native-elements'
import data from '../../assets/Countries'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
// Default render of country flag
const defaultFlag = data.filter(
    obj => obj.name === 'United States'
)[0].flag

class RegisterScreen extends React.Component {


    state = {
        flag: defaultFlag,
        modalVisible: false,
        phoneNumber: '',
        termsAndConditions :false,
        email:'',
        emailValidate : false,
        phonevalidate : true 
    }

    termandconditions(){

        this.setState({
            termsAndConditions : !this.state.termsAndConditions
        })
    }
   
    validate = (text) => {
        console.log(text);
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(text) === false) {
          console.log("Email is Not Correct");
          this.setState({ email: text, emailValidate : true })
        //  return false;
        }
        else {
          this.setState({ email: text,emailValidate : false })
          console.log("Email is Correct");
        }
      }
    onChangeText(key, value) {
        this.setState({
            [key]: value
        })
    }
    showModal() {
        console.log("log")
        this.setState({ modalVisible: true })
    }
    hideModal() {
        this.setState({ modalVisible: false })
        // Refocus on the Input field after selecting the country code
        this.refs.PhoneInput._root.focus()
    }

    gotoPassword = async() =>{

     let registerdata = {
            email : this.state.email,
            phone : this.state.phoneNumber
       }

       await AsyncStorage.setItem('registerData', JSON.stringify(registerdata))
      this.props.navigation.navigate('Password')
    }
    gotoLogin(){
      this.props.navigation.navigate('Login')
    }

    async getCountry(country) {
        const countryData = await data
        try {
            const countryCode = await countryData.filter(
                obj => obj.name === country
            )[0].dial_code
            const countryFlag = await countryData.filter(
                obj => obj.name === country
            )[0].flag
            // Set data from user choice of country
            this.setState({ phoneNumber: countryCode, flag: countryFlag })
            await this.hideModal()
        }
        catch (err) {
            console.log(err)
        }
    }

    render() {
        let { flag } = this.state
        const countryData = data
        return (
            <ScrollView>
                      <Container style={styles.containerVC}>
                                <View style={styles.centerContainer}>
                                    <Text style={text.title}>WELCOME TO THE <Text style={text.bold}> FAST SERVICES ! </Text> </Text>
                                </View>
                                <View style={styles.logoContainer}>
                                    <Image
                                        style={{
                                            width: '50%',
                                            resizeMode: 'contain'
                                        }}
                                        source={require('../../assets/images/logo.png')} />
                                </View>
                                <View style={styles.centerContainer}>
                                    <Item rounded>
                                    <Icon name='envelope' size={20} style={styles.inputicon} />
                                     <Input placeholder='EMAIL*' placeholderTextColor='#adb4bc' style={styles.emailFiled} onChangeText={(text) => this.validate(text)}
  value={this.state.email}/>
                                     
                                    </Item>
                                    { this.state.emailValidate ? <Text style={styles.erroMessage}> Please Enter correct email.</Text> : <Text style={styles.successMessage}> </Text> }
                                    
                                   </View> 
                                {/* Phone input with native-base */}
                                {/* phone section  */}
                                <Item rounded style={styles.itemStyle}>

                                    {/* country flag */}
                                    <View><Text style={{ fontSize: 25, marginLeft: 10 }} onPress={() => this.showModal()} >{flag}</Text></View>
                                    {/* open modal */}
                                    <Icon
                                        active
                                        name='angle-down'
                                        style={[styles.iconStyle, { marginLeft: 5 }]}
                                        onPress={() => this.showModal()}
                                    />
                                    <Input
                                        style={styles.input}
                                        placeholder='+15766554433'
                                        placeholderTextColor='#adb4bc'
                                        keyboardType={'phone-pad'}
                                        returnKeyType='done'
                                        autoCapitalize='none'
                                        autoCorrect={false}
                                        secureTextEntry={false}
                                        ref='PhoneInput'
                                        value={this.state.phoneNumber}
                                        onChangeText={(val) => {
                                            if (this.state.phoneNumber === '') {
                                                // render UK phone code by default when Modal is not open
                                                this.onChangeText('phoneNumber', '+1' + val)
                                            } else {
                                                // render country code based on users choice with Modal
                                                this.onChangeText('phoneNumber', val)
                                                        
                                                        console.log(val.length)

                                                       if(val.length > 9 && val.length < 14){
                                                        this.setState({
                                                            phonevalidate: true
                                                        })
                                                       }else{
                                                        this.setState({
                                                            phonevalidate: false

                                                        })
                                                       }
                                                       console.log(this.state.phonevalidate)
                                            }
                                        }
                                        }
                                    />
                                      
                                    
                                    {/* Modal for country code and flag */}
                                    <Modal
                                        animationType="slide" // fade
                                        transparent={false}
                                        visible={this.state.modalVisible}>
                                        <View style={{ flex: 1 }}>
                                            <View style={{ flex: 10, paddingTop: 30, backgroundColor: '#fff' }}>
                                                <FlatList
                                                    data={countryData}
                                                    keyExtractor={(item, index) => index.toString()}
                                                    renderItem={
                                                        ({ item }) =>
                                                            <TouchableWithoutFeedback
                                                                onPress={() => this.getCountry(item.name)}>
                                                                <View
                                                                    style={
                                                                        [
                                                                            styles.countryStyle,
                                                                            {
                                                                                flexDirection: 'row',
                                                                                alignItems: 'center',
                                                                                justifyContent: 'space-between'
                                                                            }
                                                                        ]
                                                                    }>
                                                                    <Text style={{ fontSize: 30 }}>
                                                                        {item.flag}
                                                                    </Text>
                                                                    <Text style={{ fontSize: 15, color: '#4362bf' }}>
                                                                        {item.name} ({item.dial_code})
                                    </Text>
                                                                </View>
                                                            </TouchableWithoutFeedback>
                                                    }
                                                />
                                            </View>

                                            <TouchableOpacity
                                                onPress={() => this.hideModal()}
                                                style={styles.closeButtonStyle}>
                                                <Text style={styles.textStyle}>
                                                    Close
                                              </Text>
                                            </TouchableOpacity>
                                        </View>
                                    </Modal>
                                   
                                </Item>
                                <View style={styles.phonevalidate}>
                                     { !this.state.phonevalidate ? <Text style={styles.erroMessage}> Please Enter correct Number.</Text> : <Text style={styles.successMessage}> </Text> }
                               
                                     </View>
                                <CheckBox
                                    title='ACCEPT THE TERMS AND CONDITIONS'
                                    checked={this.state.termsAndConditions}
                                    onPress={()=>this.termandconditions()}
                                />
                                {!this.state.termsAndConditions || this.state.emailValidate || this.state.email == '' || !this.state.phonevalidate ? 
                                <TouchableOpacity
                                    style={[buttons.primary, buttons.round, styles.disabled ]}
                                    activeOpacity={.5}
                                    onPress={()=>this.gotoPassword()}
                                    disabled={true}
                                >
                                    <Text style={buttons.TextStyle}> REGISTER </Text>

                                </TouchableOpacity> : 
                                <TouchableOpacity
                                style={[buttons.primary, buttons.round ]}
                                activeOpacity={.5}
                                onPress={()=>this.gotoPassword()}
                                >
                                <Text style={buttons.TextStyle}>  REGISTER </Text>

                            </TouchableOpacity>
                            }
                                <View style={styles.centerBox}>
                                    <Text style={text.lightGray}> I ALREADY HAVE AN ACCOUNT <Text style={buttons.TextStylePrimary} onPress={()=>this.gotoLogin()}> LOGIN </Text> </Text>
                                </View>
                                
                                
                                <TouchableOpacity
                                    style={[buttons.facebook, buttons.round]}
                                    activeOpacity={.5}
                                >
                                    <Text style={buttons.TextStylePrimary}> <Icon name="facebook" size={25} /> </Text>
                                    <Text style={buttons.TextStylePrimary}>  CONTINUE WITH FACEBOOK </Text>

                                </TouchableOpacity>
                            </Container>
                           
                    </ScrollView>
        )
    }
}

  export default RegisterScreen;