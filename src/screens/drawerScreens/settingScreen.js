import React, { Component } from 'react';
import { Container, Content, Text, Card, Header, Body, Button, Title, CardItem } from 'native-base';

class SettingsScreen extends Component{
  render(){
    return(
      <Container>
        <Header>
          <Body>
            <Title>Redux Counter</Title>
          </Body>
        </Header>
        <Content padder>
          <Card>
            <CardItem>
              <Text>
                {this.props.count}
              </Text>
            </CardItem>
          </Card>
        
        </Content>
      </Container>
    );
  }
}
// function mapStateToProps(state){
//   return{
//     count : state.count
//   };
// }
// function matchDispatchToProps(dispatch){
//   return bindActionCreators({increment: increment, decrement: decrement}, dispatch)
// }
// export default connect(mapStateToProps, matchDispatchToProps)(SettingsScreen);
export default SettingsScreen;
