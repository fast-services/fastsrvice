import React from 'react';
import { View, ScrollView, Text, TouchableOpacity,FlatList } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Avatar } from 'react-native-elements';
import { text, buttons, styles, cardStyle } from '../../components/styles/styles';
import Icon from 'react-native-vector-icons/FontAwesome5';
class JobViewScreen extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      dataSource: {},
      activeTab : 'pending'
    };
  }

  componentDidMount() {
    var that = this;
    let items = Array.apply(null, Array(14)).map((v, i) => {
      return { id: i};
    });
    that.setState({
      dataSource: items,
    });
  }

  requestType(type){
      this.setState({
        activeTab: type
      })
  }

  render() {
    return (
      <View>
    
        <ScrollView style={cardStyle.listView}>
       <FlatList 
        nestedScrollEnabled={true}
          data={this.state.dataSource}
          renderItem={({ item }) => ( 
         
        <View style={cardStyle.jobViewOuterBox}>
            <View style={cardStyle.card}>

               <View style={cardStyle.cardDetailBox}>
                <Text style={cardStyle.cardHeading}> SENIOR HOME DECORATORS </Text>
                <Text style={cardStyle.cardText}> <Icon name="calendar-alt"> </Icon> &nbsp; &nbsp;  CREATED WED 14, 2020</Text>
                <Text style={cardStyle.cardText}> <Icon name="comment-dollar"> </Icon> &nbsp; &nbsp; AMOUNT :  $ 30 </Text>
                <Text style={cardStyle.cardText}> <Icon name="suitcase"></Icon> &nbsp; &nbsp; JOB STATUS : <Text style={cardStyle.acceptStatusText}> ACCEPT </Text> </Text> 
   
              </View>
              
            </View>
            </View> 
          )}
           //Setting the number of column
          numColumns={1}
          keyExtractor={(item, index) => index.toString()}
        />
        </ScrollView>
      </View>
    );
  }
}

export default JobViewScreen;

