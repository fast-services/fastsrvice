import React from 'react';
import { View, ScrollView, Text, TouchableOpacity,FlatList } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Avatar } from 'react-native-elements';
import { text, buttons, styles, cardStyle } from '../../components/styles/styles';
import Icon from 'react-native-vector-icons/FontAwesome5';
class JobScreen extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      dataSource: {},
      activeTab : 'pending'
    };
  }

  componentDidMount() {
    var that = this;
    let items = Array.apply(null, Array(14)).map((v, i) => {
      return { id: i};
    });
    that.setState({
      dataSource: items,
    });
  }

  requestType(type){
      this.setState({
        activeTab: type
      })
  }
  viewJob(){
    console.log("Props","Go to view")
    this.props.navigation.navigate('JobViewScreen');
  }

  render() {
    return (
      <View>
    
        <ScrollView >
       <FlatList 
        nestedScrollEnabled={true}
          data={this.state.dataSource}
          renderItem={({ item }) => ( 
         
           
            <TouchableOpacity style={cardStyle.jobCard} onPress={()=>this.viewJob(item.id)}>

              <Avatar
                rounded
                size="medium"
                source={{
                  uri:
                    'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
                }}
              />
              <View style={cardStyle.cardDetailBox}>
                <Text style={cardStyle.jobsCardText}>SENIOR HOME DECORATORS </Text>
                <Text style={cardStyle.jobDescriptionText}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </Text>
               <View style={cardStyle.jobFooter}>
                 <Text style={[cardStyle.historyCardText, cardStyle.jobDescription ]}> 
                    <Icon name="clock"/>  27/05/2020 
                 </Text>
                 <Text style={[cardStyle.historyCardText, cardStyle.jobDescription ]}> 
                    <Icon name="suitcase"/>  FULL TIME
                 </Text>
               </View>
              </View>
  
            </TouchableOpacity>
          
          )}
           //Setting the number of column
          numColumns={1}
          keyExtractor={(item, index) => index.toString()}
        />
        </ScrollView>
      </View>
    );
  }
}

export default JobScreen;

