import React from 'react';
import { View, ScrollView, Text, TouchableOpacity,FlatList } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Avatar } from 'react-native-elements';
import { text, buttons, styles, cardStyle } from '../../components/styles/styles';
import Icon from 'react-native-vector-icons/FontAwesome5';
class HistoryScreen extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      dataSource: {},
      activeTab : 'pending'
    };
  }

  componentDidMount() {
    var that = this;
    let items = Array.apply(null, Array(14)).map((v, i) => {
      return { id: i};
    });
    that.setState({
      dataSource: items,
    });
  }

  requestType(type){
      this.setState({
        activeTab: type
      })
  }

  render() {
    return (
      <View>
    
        <ScrollView >
       <FlatList 
        nestedScrollEnabled={true}
          data={this.state.dataSource}
          renderItem={({ item }) => ( 
         
           
            <View style={cardStyle.historyCard}>

              <Avatar
                rounded
                size="medium"
                source={{
                  uri:
                    'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
                }}
              />
              <View style={cardStyle.cardDetailBox}>
                <Text style={cardStyle.historyCardText}> MATT M.WHITE</Text>
                <Text style={cardStyle.historyCardText}> 879 MULBERRY,76  </Text>
              </View>
              <View>
                 <TouchableOpacity style={[cardStyle.acceptButton, cardStyle.cartButton]}>
                    <Text style={cardStyle.cardButtonText}>
                      SUCCESS
                     </Text>
                  </TouchableOpacity>
                  <Text style={[cardStyle.historyCardText,cardStyle.payment]}><Icon style={cardStyle.moneyIcon} name="coins"/> $ 30 CASH </Text>

              </View>
              
            </View>
          
          )}
           //Setting the number of column
          numColumns={1}
          keyExtractor={(item, index) => index.toString()}
        />
        </ScrollView>
      </View>
    );
  }
}

export default HistoryScreen;

