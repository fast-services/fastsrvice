import React from 'react'
import {
  View,
  Text,
  Modal,
  FlatList,
  StyleSheet,
  SafeAreaView,
  Keyboard,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Image,
  ScrollView,
  Button
} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import { text, buttons, styles } from '../../components/styles/styles';
//import Icon from 'react-native-vector-icons/FontAwesome';
// native base imports
import { Container, Header, Content, Form, Item, Input, Label, Icon } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import { Overlay } from 'react-native-elements';
import axios from '../../server/apis'
import {
  updatePicture
} from '../../store/actions'


class ProfileScreen extends React.Component {
  state = {
    photo: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      filePath: {},
      dataSource: {},
      SubservicedataSource: {},
      selectedItem:0,
      subServiceVisible : false,
      pic : '',
      userdata : {}
    };
  }
  
  componentDidMount () {
    var that = this;
    this.getUserdata()
    console.log("pik =>", this.state.pic)
    this.getAllService()
  }

   getAllService  = async () => {

    const response = await axios.get(`/servicesList`)
       
    console.log(response.data);
          if (response.data.success) {
            this.setState({
              dataSource: response.data.services_list,
            });
           
      }else{
         
      }
   }

   selectSubService(){

   }
   getAllSubservices  = async (serviceId) => {

    let body = {
        "service_id":serviceId
    }

    const response = await axios.post(`/serviceTypeList`, body)
       
    console.log(response.data);
          if (response.data.success) {
            this.setState({
              SubservicedataSource: response.data.services_list,
            });
           
      }else{
         
      }
   }

  getUserdata = async () => {
    let  userdata =  await AsyncStorage.getItem('user')
    var user = JSON.parse(userdata);
    console.log(user.image)
    this.setState({
      pic : user.image,
      userdata: user
    });

    console.log(this.state.pic)
  }

  chooseFile = () => {
    var options = {
      title: 'Select Image',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
    //  console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        let source = response;
        // You can also display the image using data:
        //  let source = { uri: 'data:image/jpeg;base64,' + response.data };
        this.setState({
          filePath: source,
        });

        this.props.updatePicture('data:image/jpeg;base64,' + response.data )
      }
    });
  };

  validate() {
    console.log("ok")
  }

  selectService(serviceId){
    console.log("current service item",serviceId)
      this.setState({
        selectedItem : serviceId,
        subServiceVisible : true
      })    
      
      this.getAllSubservices(serviceId) 
  }
  profileUpdate(){
    console.log("Profile update Done.")
  }
  toggleOverlay(){
     this.setState({
      subServiceVisible : false
     })
  }
  

  render() {
    return (
       <ScrollView>
       <View style={styles.formContainer}>
              <View style={styles.centerContainer}>
                <TouchableOpacity  onPress={this.chooseFile.bind(this)}>
                 {this.state.filePath.data ? <Image
            source={{
              uri: 'data:image/jpeg;base64,' + this.state.filePath.data,
            }}
            style={{ width: 100, height: 100, borderRadius: 50}}  /> : this.state.pic.length > 0  ? <Image 
            source={{
              uri: this.state.pic,
            }}source={{
              uri: this.state.pic,
            }}
            style={{width: 100, height: 100,  borderRadius: 50 }}  /> :
            <Image 
            source={require('../../../assets/images/avetar.png')}
            style={{width: 100, height: 100,  borderRadius: 50  }}  />
            }
                  
            </TouchableOpacity>
                <Text style={[text.title,text.bold]}> EDIT PROFILE  </Text>
          </View>
          
                <Item rounded style={styles.formFiled}>
                  <Icon type="FontAwesome" name="user-circle-o"  style={styles.formFiledIcon} />
                  <Input placeholder='FIRST NAME' placeholderTextColor='#adb4bc' onChangeText={(text) => this.validate(text)}  value={this.state.userdata.first_name}/>
                </Item>
                <Item rounded style={styles.formFiled}>
                  <Icon type="FontAwesome" name="user-circle-o"  style={styles.formFiledIcon} />
                  <Input placeholder='LAST NAME' placeholderTextColor='#adb4bc' onChangeText={(text) => this.validate(text)}  value={this.state.userdata.last_name}/>
                </Item>
                <Item rounded style={styles.formFiled}>
                  <Icon type="FontAwesome" name="envelope"  style={styles.formFiledIcon}  />
                  <Input placeholder='EMAIL' placeholderTextColor='#adb4bc' onChangeText={(text) => this.validate(text)}  value={this.state.userdata.email} />
                </Item>
                <Item rounded style={styles.formFiled}>
                  <Icon type="FontAwesome" name="phone"  style={styles.formFiledIcon}  />
                  <Input placeholder='CONTACT NUMBER' placeholderTextColor='#adb4bc' onChangeText={(text) => this.validate(text)}  value={this.state.userdata.phone} />
                </Item>
                <Item rounded style={styles.formFiled}>
                  <Icon type="FontAwesome" name="file-text"  style={styles.formFiledIcon} />
                  <Input placeholder='DESCRIBE YOUR SELF' placeholderTextColor='#adb4bc' onChangeText={(text) => this.validate(text)}  value={this.state.userdata.bio} />
                </Item>
                <Item rounded style={styles.formFiled}>
                  <Icon type="FontAwesome" name="map-marker"  style={styles.formFiledIcon}/>
                  <Input placeholder='ADDRESS' placeholderTextColor='#adb4bc'  onChangeText={(text) => this.validate(text)}  value={this.state.userdata.address}/>
                </Item>
              <View style={styles.listviewbox}>

              {this.state.subServiceVisible ? 

         <FlatList
                nestedScrollEnabled={true}
          data={this.state.SubservicedataSource}
          renderItem={({ item }) => (
            <TouchableOpacity style={{ flex: 1, flexDirection: 'column', margin: 5, borderRadius:5 }} onPress={()=>this.selectSubService(item.id)}>
               <View style={[styles.serviceItem, item.id == this.state.selectedItem ? styles.activeItem : styles.normalItem ] }>
               <Image 
           source={{
            uri: item.image
          }}
            style={{width: 40, height: 40,  borderRadius: 50}}  />
                  <Text style={[styles.serviceItemText,  item.id == this.state.selectedItem ? styles.activeItemText : styles.normalItem ]}> {item.service_type_name} </Text>
              </View>
            </TouchableOpacity>
          )}
          //Setting the number of column
          numColumns={3}
          keyExtractor={(item, index) => index.toString()}
        /> : <FlatList 
        nestedScrollEnabled={true}
  data={this.state.dataSource}
  renderItem={({ item }) => (
    <TouchableOpacity style={{ flex: 1, flexDirection: 'column', margin: 5, borderRadius:5 }} onPress={()=>this.selectService(item.id)}>
       <View style={[styles.serviceItem, item.id == this.state.selectedItem ? styles.activeItem : styles.normalItem ] }>
       <Image 
   source={{
    uri: item.image
  }}
    style={{width: 40, height: 40,  borderRadius: 3}}  />
          <Text style={[styles.serviceItemText,  item.id == this.state.selectedItem ? styles.activeItemText : styles.normalItem ]}> {item.name} </Text>
      </View>
    </TouchableOpacity>
  )}
  //Setting the number of column
  numColumns={3}
  keyExtractor={(item, index) => index.toString()}
/> }
        </View>
        <TouchableOpacity
                                style={[buttons.primary, buttons.round]}
                                activeOpacity={.5}
                                onPress={()=>this.profileUpdate()}
                            >
                               <Text style={buttons.TextStyle}> UPDATE PROFILE {this.state.value}</Text> 

                                    </TouchableOpacity>
           </View>
    
           </ScrollView>   
    )
  }
}
const mapStateToProps = (state) => {
  return state
}

const mapDispatchToProps = (dispatch) => {
  return {
    updatePicture: (data) => dispatch(updatePicture(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen)