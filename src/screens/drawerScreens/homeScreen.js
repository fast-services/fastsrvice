import React from 'react';
import { View, ScrollView, Text, TouchableOpacity,FlatList } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Avatar } from 'react-native-elements';
import { text, buttons, styles, cardStyle } from '../../components/styles/styles';
import axios from '../../server/apis'
import AsyncStorage from '@react-native-community/async-storage'
class HomeScreen extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      dataSource: {},
      activeTab : 'pending',
      proiver_id : ''
    };
  }

  componentDidMount() {
     this.getAllRequest()
    // this.updateRequest()
     console.log(this.state.dataSource.length)
  }

  getAllRequest = async () =>{
    this.setState({
      dataSource: {},
    });  
    let  userdata =  await AsyncStorage.getItem('user')
   let token  = await AsyncStorage.getItem('token')
  //  console.log("token",token)
     //  console.log(userdata)

       var obj = JSON.parse(userdata);
          
      this.setState({
        proiver_id: obj.id
      })
     
   let type = 0;
      if(this.state.activeTab == 'accepted'){
        type  = 1
      }else if(this.state.activeTab == 'pending'){
        type  = 0
      }
      else if(this.state.activeTab == 'cancel'){
        type  = 2
      }
      else if(this.state.activeTab == 'complete'){
        type  = 3
      }

    let body = {
      provider_id :  1,  
      request_status_id : type
    }
     console.log("body", body)

    const response = await axios.post(`/listRequest`, body)

    console.log(response.data.success)
      
    if (response.data.success) {
         console.log(response.data.request_details)
         this.setState({
          dataSource: response.data.request_details,
        });  
    }else{
      this.setState({
        dataSource: {},
      });  
    }
  }

  updateRequest = async (requestId, status)=>{
     console.log("change request", requestId, status) 

     let body = {
      serviceRequestId: requestId, 
      provider_id: "1",   
      request_status: status
    }
    // this.state.proiver_id    // This will replace when use app is implementad.
    const response = await axios.put(`/updateRequest`, body)
      console.log(response.data)
    if (response.data.success) {
      this.getAllRequest()
    }
    
  }

  requestType(type){
      this.setState({
        activeTab: type
      },() => {
        this.getAllRequest();
    })
  }

  render() {
    return (
      <View>
        <View style={cardStyle.filterBox}>
           <TouchableOpacity style={this.state.activeTab == 'accepted'? cardStyle.activerequestFilter:cardStyle.requestFilter} onPress={()=>this.requestType('accepted')}>
             <Text style={this.state.activeTab == 'accepted'? cardStyle.activefilterText:cardStyle.filterText  }> ACCEPTED </Text> 
           </TouchableOpacity>
           <TouchableOpacity style={this.state.activeTab == 'pending'? cardStyle.activerequestFilter:cardStyle.requestFilter} onPress={()=>this.requestType('pending')}>
             <Text style={this.state.activeTab == 'pending'? cardStyle.activefilterText:cardStyle.filterText  }> PENDING </Text> 
           </TouchableOpacity>
           <TouchableOpacity style={this.state.activeTab == 'cancel'? cardStyle.activerequestFilter:cardStyle.requestFilter} onPress={()=>this.requestType('cancel')}>
             <Text style={this.state.activeTab == 'cancel'? cardStyle.activefilterText:cardStyle.filterText  }> CANCELED </Text> 
           </TouchableOpacity>
           <TouchableOpacity style={this.state.activeTab == 'complete'? cardStyle.activerequestFilter:cardStyle.requestFilter} onPress={()=>this.requestType('complete')}>
             <Text style={this.state.activeTab == 'complete'? cardStyle.activefilterText:cardStyle.filterText  }> COMPLETED </Text> 
           </TouchableOpacity>
        </View>
        <ScrollView style={cardStyle.listView}>
       { this.state.activeTab == 'accepted'? <FlatList 
        nestedScrollEnabled={true}
          data={this.state.dataSource}
          renderItem={({ item }) => ( 
          <View style={cardStyle.OuterBox}>
           
            <View style={cardStyle.card}>

            { item.requester_image == null ? <Avatar
                rounded
                size="medium"
                title={item.service_type_name[0]}
                source={require('../../../assets/images/dummy.jpg')}
              /> : <Avatar
              rounded
              size="medium"
              source={{
                uri:
                item.requester_image,
              }}
            /> }
              <View style={cardStyle.cardDetailBox}>
              <Text style={cardStyle.cardText}> SERVICE : {item.service_type_name}  </Text>
              <Text style={cardStyle.cardText}> BUDGET : ${item.budget} </Text>
              <Text style={cardStyle.cardText}> PAYMENT TYPE : {item.payment_type == 1 ? 'CASH': item.payment_type == 2 ? 'CARD' : 'PAYPAL' } </Text>
                <Text style={cardStyle.cardText}> DATE : {item.created_at}  </Text>

                <View style={cardStyle.buttonBox}>
                  <TouchableOpacity style={[cardStyle.acceptButton, cardStyle.cartButton]}>
                    <Text style={cardStyle.cardButtonText}>
                      VIEW DETAILS
                               </Text>
                  </TouchableOpacity>

                  <TouchableOpacity style={[cardStyle.cancelButton, cardStyle.cartButton]} onPress={()=>this.updateRequest(item.id, 2)}>
                    <Text style={cardStyle.cancelButtonText}>
                      CANCEL
                               </Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={[cardStyle.cancelButton, cardStyle.cartButton, cardStyle.cardStatus]}>
              <Text style={cardStyle.statusText}>
                REQUEST ACCEPTED
              </Text>
            </View>
            </View>
          </View>
          )}
           //Setting the number of column
          numColumns={1}
          keyExtractor={(item, index) => index.toString()}
        /> : <Text style={cardStyle.blankBox}>asddsa</Text> }


{ this.state.activeTab == 'cancel'? <FlatList 
        nestedScrollEnabled={true}
          data={this.state.dataSource}
          renderItem={({ item }) => ( 
          <View style={cardStyle.OuterBox}>
           
            <View style={cardStyle.card}>

            { item.requester_image == null ? <Avatar
                rounded
                size="medium"
                title={item.service_type_name[0]}
                source={require('../../../assets/images/dummy.jpg')}
              /> : <Avatar
              rounded
              size="medium"
              source={{
                uri:
                item.requester_image,
              }}
            /> }
              <View style={cardStyle.cardDetailBox}>
              <Text style={cardStyle.cardText}> SERVICE : {item.service_type_name}  </Text>
              <Text style={cardStyle.cardText}> BUDGET : ${item.budget} </Text>
              <Text style={cardStyle.cardText}> PAYMENT TYPE : {item.payment_type == 1 ? 'CASH': item.payment_type == 2 ? 'CARD' : 'PAYPAL' } </Text>
                <Text style={cardStyle.cardText}> DATE : {item.created_at}  </Text>

              
              </View>
              <View style={[cardStyle.cancelButton, cardStyle.cartButton, cardStyle.cardStatus]}>
              <Text style={cardStyle.statusText}>
                REQUEST CANCELED
              </Text>
            </View>
            </View>
          </View>
          )}
           //Setting the number of column
          numColumns={1}
          keyExtractor={(item, index) => index.toString()}
        /> : <Text style={cardStyle.blankBox}> sdd</Text> }
        { this.state.activeTab == 'pending'? <FlatList 
        nestedScrollEnabled={true}
          data={this.state.dataSource}
          renderItem={({ item }) => ( 
          <View style={cardStyle.OuterBox}>
           
            <View style={cardStyle.card}>

            { item.requester_image == null ? <Avatar
                rounded
                size="medium"
                title={item.service_type_name[0]}
                source={require('../../../assets/images/dummy.jpg')}
              /> : <Avatar
              rounded
              size="medium"
              source={{
                uri:
                item.requester_image,
              }}
            /> }
              <View style={cardStyle.cardDetailBox}>
              <Text style={cardStyle.cardText}> SERVICE : {item.service_type_name}  </Text>
              <Text style={cardStyle.cardText}> BUDGET : ${item.budget} </Text>
              <Text style={cardStyle.cardText}> PAYMENT TYPE : {item.payment_type == 1 ? 'CASH': item.payment_type == 2 ? 'CARD' : 'PAYPAL' } </Text>
                <Text style={cardStyle.cardText}> DATE : {item.created_at}  </Text>

                <View style={cardStyle.buttonBox}>
                  <TouchableOpacity style={[cardStyle.acceptButton, cardStyle.cartButton]} onPress={()=>this.updateRequest(item.id, 1)}>
                    <Text style={cardStyle.cardButtonText}>
                      ACCEPT
                               </Text>
                  </TouchableOpacity>

                  <TouchableOpacity style={[cardStyle.cancelButton, cardStyle.cartButton]} onPress={()=>this.updateRequest(item.id, 2)}>
                    <Text style={cardStyle.cancelButtonText}>
                      CANCEL
                               </Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={[cardStyle.cancelButton, cardStyle.cartButton, cardStyle.cardStatus]}>
              <Text style={cardStyle.statusText}>
                REQUEST PENDING
              </Text>
            </View>
            </View>
          </View>
          )}
           //Setting the number of column
          numColumns={1}
          keyExtractor={(item, index) => index.toString()}
        /> : <Text style={cardStyle.blankBox}>dasdsa</Text> }

        

{ this.state.activeTab == 'complete'? <FlatList 
        nestedScrollEnabled={true}
          data={this.state.dataSource}
          renderItem={({ item }) => ( 
          <View style={cardStyle.OuterBox}>
           
            <View style={cardStyle.card}>

            { item.requester_image == null ? <Avatar
                rounded
                size="medium"
                title={item.service_type_name[0]}
                source={require('../../../assets/images/dummy.jpg')}
              /> : <Avatar
              rounded
              size="medium"
              source={{
                uri:
                item.requester_image,
              }}
            /> }
              <View style={cardStyle.cardDetailBox}>
              <Text style={cardStyle.cardText}> SERVICE : {item.service_type_name}  </Text>
              <Text style={cardStyle.cardText}> BUDGET : ${item.budget} </Text>
              <Text style={cardStyle.cardText}> PAYMENT TYPE : {item.payment_type == 1 ? 'CASH': item.payment_type == 2 ? 'CARD' : 'PAYPAL' } </Text>
                <Text style={cardStyle.cardText}> DATE : {item.created_at}  </Text>

              </View>
              <View style={[cardStyle.cancelButton, cardStyle.cartButton, cardStyle.cardStatus]}>
              <Text style={cardStyle.statusText}>
                REQUEST COMPLETED
              </Text>
            </View>
            </View>
          </View>
          )}
           //Setting the number of column
          numColumns={1}
          keyExtractor={(item, index) => index.toString()}
        /> : <Text style={cardStyle.blank}></Text> }
       
        </ScrollView>
      </View>
    );
  }
}

export default HomeScreen;

