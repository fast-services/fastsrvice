import React, { useState, useEffect } from 'react';
import axios, {setClientToken } from '../server/apis'
//Import all required component
import { ActivityIndicator, View, StyleSheet, Image, Text } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';


const styles = StyleSheet.create({
  title: {
    color: '#4362bf',
    fontFamily: 'OpenSans-Regular',
    fontSize: 18
  },
  bold :{
    fontWeight:"bold"
  }
});

const SplashScreen = props => {
  //State for ActivityIndicator animation
  let [animating, setAnimating] = useState(true);

 
  
  useEffect(() => {
    setTimeout( async() => {
      let token = await AsyncStorage.getItem('token');
      setClientToken(token)
      setAnimating(false);
      //Check if user_id is set or not
      //If not then send for Authentication
      //else send to Home Screen
      await AsyncStorage.getItem('token').then(value =>
        
       props.navigation.navigate(
          value === null ? 'Auth' : 'DrawerNavigationRoutes'
        )
        
      );
    }, 5000);
  }, []);

  return (
    <View style={{
      flex: 1,
      width: '100%',
      height: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      paddingHorizontal: 20,
  }}>
      <Image
          style={{
              width: '50%',
              marginHorizontal: 50,
              resizeMode: 'contain'
          }}
          source={require('../../assets/images/logo.png')} />
         <Text style={styles.title}> <Text style={styles.bold}> FAST </Text> SERVICES </Text>
  </View>
  );
};
export default SplashScreen;
