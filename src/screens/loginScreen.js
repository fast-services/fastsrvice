import React from 'react'
import {
    View,
    Text,
    Modal,
    FlatList,
    StyleSheet,
    SafeAreaView,
    Keyboard,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Image,
    ScrollView
} from 'react-native'
import { connect } from 'react-redux'
import { text, buttons, styles } from '../components/styles/styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import Axios, {setClientToken} from '../server/apis'
import {
    loginUser
} from '../store/actions'
// native base imports
import {
    Container,
    Item,
    Input,
    ListItem, Body
} from 'native-base'
import { CheckBox } from 'react-native-elements'
import data from '../../assets/Countries'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
// Default render of country flag
const defaultFlag = data.filter(
    obj => obj.name === 'United States'
)[0].flag

class LoginScreen extends React.Component {

  
    state = {
        flag: defaultFlag,
        modalVisible: false,
        phoneNumber: '',
        termsAndConditions :true,
        hidePassword : true,
        icon : 'eye-slash',
        password : '',
        email: '',
        errors: {},
        isAuthorized: false,
        isLoading: false,
    }
   
    validate = (text) => {
        console.log(text);
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(text) === false) {
          console.log("Email is Not Correct");
          this.setState({ email: text, emailValidate : true })
        //  return false;
        }
        else {
          this.setState({ email: text,emailValidate : false })
          console.log("Email is Correct");
        }
      }

      password=(text)=>{
        console.log(text);
        this.setState({ password: text })
      }
     
      
    showModal() {
        this.setState({ modalVisible: true })
    }
    hideModal() {
        this.setState({ modalVisible: false })
        // Refocus on the Input field after selecting the country code
        this.refs.PhoneInput._root.focus()
    }
    show(){
        this.setState({
          hidePassword : !this.state.hidePassword
        },() =>{
            if(this.state.hidePassword){
              this.setState({
                  icon : 'eye-slash'
                })
            }else{
              this.setState({
                  icon : 'eye'
                })
            }
          })
     }

  forgotPassword(){
    this.props.navigation.navigate('SendCode')
  } 

  onPressLogin() {
    const {email, password} = this.state;
    const payload = {email, password};
    this.props.loginUser( payload, this.props.navigation)
  }

    async getCountry(country) {
        const countryData = await data
        try {
            const countryCode = await countryData.filter(
                obj => obj.name === country
            )[0].dial_code
            const countryFlag = await countryData.filter(
                obj => obj.name === country
            )[0].flag
            // Set data from user choice of country
            this.setState({ phoneNumber: countryCode, flag: countryFlag })
            await this.hideModal()
        }
        catch (err) {
            console.log(err)
        }
    }

    render() {
        let { flag } = this.state
        const countryData = data
        return (
            
                         <KeyboardAwareScrollView>
                            <Container style={styles.containerVC}>
                                <View style={styles.centerContainer}>
                                    <Text style={text.title}>WELCOME TO THE <Text style={text.bold}> FAST SERVICES ! </Text> </Text>
                                </View>
                                <View style={styles.logoContainer}>
                                    <Image
                                        style={{
                                            width: '50%',
                                            resizeMode: 'contain'
                                        }}
                                        source={require('../../assets/images/logo.png')} />
                                </View>
                                <View style={styles.centerContainer}>
                                    <Item rounded>
                                    <Icon name='envelope' size={20} style={styles.inputicon} />
                                     <Input placeholder='EMAIL*' placeholderTextColor='#adb4bc' style={styles.emailFiled} onChangeText={(text) => this.validate(text)}
  value={this.state.email}/>
                                     
                                    </Item>
                                    { this.state.emailValidate ? <Text style={styles.erroMessage}> Please Enter correct email.</Text> : <Text style={styles.successMessage}> </Text> }
                                    
                                   </View> 
                                <Item rounded>
                                <Icon name='lock' size={20} style={styles.inputicon} />
                                    
                                     <Input placeholder='ENTER PASSWORD' placeholderTextColor='#adb4bc' style={styles.passwordFiled} secureTextEntry={this.state.hidePassword} onChangeText={(text) => this.password(text)}
  value={this.state.password} />
                                     <TouchableOpacity onPress={()=>this.show()}>
                                      <Icon name={this.state.icon} size={20} style={styles.passwordLock} />
                                     </TouchableOpacity>
                                    </Item>

                                    { this.state.emailValidate || this.state.email =='' || this.state.password == '' ?  <TouchableOpacity
                                    style={[buttons.primary, buttons.round, styles.disabled]}
                                    activeOpacity={.5}
                                    disabled
                                >
                                   <Text style={buttons.TextStyle}> LOGIN </Text> 

                                </TouchableOpacity> : 
                                <TouchableOpacity
                                style={[buttons.primary, buttons.round]}
                                activeOpacity={.5}
                                onPress={()=>this.onPressLogin()}
                            >
                               <Text style={buttons.TextStyle}> LOGIN </Text> 

                                    </TouchableOpacity> }
                                
                                <View style={styles.centerBox}>
                                    <Text style={text.lightGray}> OR </Text>
                                </View>
                                {/* <TouchableOpacity
                                    style={[buttons.facebook, buttons.round]}
                                    activeOpacity={.5}
                                >
                                    <Text style={buttons.TextStylePrimary}> <Icon name="facebook" size={25} /> </Text>
                                    <Text style={buttons.TextStylePrimary}>  CONTINUE WITH FACEBOOK </Text>

                                </TouchableOpacity> */}
                                <TouchableOpacity
                                    style={[buttons.facebook, buttons.round]}
                                    activeOpacity={.5}
                                    onPress={()=>this.forgotPassword()}
                                >
                                    <Text style={buttons.TextStylePrimary}> <Icon name="unlock-alt" size={25} /> </Text>
                                    <Text style={buttons.TextStylePrimary}> I FORGOT MY PASSWORD </Text>

                                </TouchableOpacity>
                            </Container>
                </KeyboardAwareScrollView>
        )
    }
}

const mapStateToProps = (state) => {
    return state
}

const mapDispatchToProps = (dispatch) => {
    return {
        loginUser: (credentials, navigation) => dispatch(loginUser(credentials, navigation))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)