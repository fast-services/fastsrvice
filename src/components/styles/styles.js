import { StyleSheet } from 'react-native'
import { colorWhite, colorPrimary, colorAccent, colorLight } from './colorStyle'
import { Overlay } from 'react-native-elements'
import { Row } from 'native-base'

const text = StyleSheet.create({
  title: {
    color: colorPrimary,
    fontFamily: 'OpenSans-Regular',
    fontSize: 18
  },
  bold: {
    fontWeight: "bold"
  },
  lightGray: {
    color: colorAccent,
  },
  heading: {
    fontSize: 22,
    color: colorPrimary,
  }
})

const buttons = StyleSheet.create({
  primary: {
    marginTop: 10,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: colorPrimary,
    alignItems: 'center',
    justifyContent: 'center',
    color: "#fff"
  },
  facebook: {
    marginTop: 10,
    paddingTop: 25,
    paddingBottom: 25,
    backgroundColor: colorWhite,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: colorPrimary,
    borderWidth: 1,
    flexDirection: "row",
    height: 50,
    fontFamily: 'OpenSans-Regular',
  },
  round: {
    borderRadius: 30
  },
  TextStyle: {
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'OpenSans-Regular',
  },
  TextStylePrimary: {
    color: colorPrimary,
    textAlign: 'center',
    fontWeight: 'bold',
    fontFamily: 'OpenSans-Regular',
  }
})


const styles = StyleSheet.create({

  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
  phonevalidate : {
   
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection:"row",
    paddingBottom:10
  },
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 0,
    backgroundColor: "#fff"
  },
  containerVC: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center',
    paddingHorizontal: 20
  },
  logoContainer: {
    position: 'relative',
    height: 140,
    alignItems: 'center',
    justifyContent: 'center'
  },
  centerContainer: {
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
    width: "100%",
    paddingTop: 10,
    paddingBottom: 10,
  },
  leftContainer: {
    flex: 1,
    flexDirection: 'column',
    position: 'relative',
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingTop: 25,
    paddingBottom: 25,
    textAlign: "left"
  },
  centerBox: {
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
    top: 5
  },
  logo: {
    fontSize: 50,
    letterSpacing: 5,
    color: colorWhite
  },
  heading: {
    fontSize: 25,
    color: colorAccent,
    marginTop: 30,
    letterSpacing: 1,
    width: '100%',
    textAlign: 'center',
    paddingBottom: 10,
  },
  formContainer: {
    flex: 1,
    width: '100%',
    paddingHorizontal: 10,
    height:'100%',
  },
  input: {
    flex: 1,
    fontSize: 15,

    color: '#000',
  },
  infoContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 30,
  },
  itemStyle: {
    marginBottom: 10,
  },
  iconStyle: {
    color: colorPrimary,
    fontSize: 28,
    marginLeft: 15
  },
  buttonStyle: {
    alignItems: 'center',
    backgroundColor: colorPrimary,
    padding: 14,
    marginBottom: 10,
    borderRadius: 3,
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: "#000",
  },
  textStyle: {
    padding: 5,
    fontSize: 20,
    color: '#fff',
    fontWeight: 'bold'
  },
  countryStyle: {
    flex: 1,
    backgroundColor: "#fff",
    borderTopColor: '#ccc',
    borderTopWidth: 1,
    padding: 8,
    color: "#ccc"
  },
  closeButtonStyle: {
    flex: 1,
    padding: 8,
    alignItems: 'center',
    backgroundColor: colorPrimary
  },
  mainContainer: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
    alignContent: "center",
    justifyContent: 'center',
  },
  passwordLock: {
    position: 'relative',
    marginRight: 10,
    color: colorPrimary
  },
  inputicon: {
    position: 'relative',
    marginLeft: 10,
    color: colorPrimary

  },
  passwordFiled: {
    fontSize: 15,
    color: "#525252"
  },
  emailFiled: {
    fontSize: 15,
    color: "#525252",
    paddingLeft: 15,
  },
  borderStyleBase: {
    width: 45,
    height: 45
  },

  borderStyleHighLighted: {
    borderColor: "#03DAC6",
  },

  underlineStyleBase: {
    width: 45,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 1,
    backgroundColor: '#e8e9ee',
    color: colorPrimary
  },

  underlineStyleHighLighted: {
    borderColor: colorPrimary
  },
  erroMessage: {
    color: '#f88',
    position: 'relative',
    top: 5
  },
  successMessage: {
    padding: 0,
    height: 0
  },
  disabled: {
    opacity: 0.5
  },
  formContainer: {
    padding: 15
  },
  formFiled: {
    backgroundColor: "#fff",
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,  
    elevation: 5,
    marginTop: 10,
    color: "#adb4bc",
    fontSize:15,
  },
  formFiledIcon:{
   color : '#6b6b6b',
   fontSize:15,
   marginLeft:10,
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
  },
  MainContainer: {
    justifyContent: 'center',
    flex: 1,
     paddingHorizontal: 20,
    paddingVertical: 10,
    height:400
  },
  listviewbox:{
    height: 300,
    paddingHorizontal: 10,
    paddingVertical: 20,
    overflow:"scroll"
  },
  serviceItem:{
    textAlign: "center",
    backgroundColor: colorPrimary,
    alignItems: 'center',
    padding: 10,
    color: colorWhite,
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,  
    elevation: 5,
    borderRadius:10
  },
  serviceItemText:{
     color: colorWhite,
     textAlign:"center",
     fontFamily: 'OpenSans-Regular',
     fontSize: 11,
     fontWeight:'bold'
  },
  ServiceIcons:{
    color: colorWhite
  },
  activeItem :{
   backgroundColor: "#fff",
  },
  activeItemIcon :{
    color: colorPrimary,
   },
   activeItemText :{
    color: colorPrimary,
   },
   subserrvicePopup:{
     position:'absolute',
     top: 0,
     left: 0,
     flex:1,
     height: '100%',
     width: '100%',
     backgroundColor:'#ccc',
     zIndex:999999
   }
})
const cardStyle = StyleSheet.create({

  card:{
       flex:1,
       flexDirection:"row",
       backgroundColor: colorPrimary,
       padding: 30,
       borderRadius: 8,
       
      
  },
  cardText:{
    color: colorWhite,
     textAlign:"center",
     fontFamily: 'OpenSans-Regular',
     fontSize: 11,
     fontWeight:'bold'
  },
  cardDetailBox:{
    textAlign:"left",
    flex:1,
    alignItems:"flex-start",
    paddingLeft:20, 
  },
  buttonBox:{
    flex : 1,
    flexDirection: "row",
    paddingTop : 15 
  },
  cartButton: {
    paddingLeft:15,
    paddingRight:15,
    paddingTop:5,
    paddingBottom:5,
    borderRadius: 50,
    borderColor: colorWhite,
    borderWidth:1,
    marginRight:10,
    color: colorWhite
  },
  cardButtonText:{
 color: colorWhite
  },
  acceptButton:{
    backgroundColor:colorPrimary,

  },
  cancelButton:{
   backgroundColor:colorWhite,
   fontWeight: 'bold',
   fontFamily: 'OpenSans-Regular',
   fontWeight: 'bold',
    fontSize:12,
  },
  cancelButtonText:{
    color: colorPrimary,
    fontWeight: 'bold',
    fontFamily: 'OpenSans-Regular',
    fontWeight: 'bold',
    fontSize:12,
  },
  cardStatus:{
    position:'absolute',
    top: -15,
    right:5,
     shadowColor: '#000',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,  
    elevation: 5,
  },
  OuterBox:{
    paddingTop:40,
    zIndex:999,
  },
  statusText:{
    color: colorPrimary,
    fontWeight: 'bold',
    fontSize:12,
    fontFamily: 'OpenSans-Regular',
  },
  filterBox:{
    flexDirection:"row",
    justifyContent:'space-between',
    alignItems: 'stretch',
    backgroundColor:colorWhite,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 40,
    elevation: 5,
   
  },
  requestFilter:{
    padding:12,
    shadowColor: '#ccc',
  },
  filterText:{
    color: colorPrimary,
    fontWeight: 'bold',
    fontSize:12,
    fontFamily: 'OpenSans-Regular'
  },
  activerequestFilter:{
    padding:12,
    shadowColor: '#ccc',
    backgroundColor: colorPrimary,
    borderTopColor:'#fff',
    borderTopWidth:1
  },
  activefilterText:{
    color: colorWhite,
    fontWeight: 'bold',
    fontSize:12,
    fontFamily: 'OpenSans-Regular'
  },
  listView:{
    paddingHorizontal:15
  },
  blankBox:{
    height: 0,
    display:'none',
    lineHeight: 0,
    fontSize:0
  },
  historyCard:{
    flex:1,
    flexDirection:"row",
    backgroundColor: colorWhite,
    padding: 30,
    borderRadius: 8,
    borderBottomWidth:1,
    borderBottomColor: colorPrimary,
    alignItems:"center"
  },
  historyCardText:{
    color: colorPrimary,
     textAlign:"center",
     fontFamily: 'OpenSans-Regular',
     fontSize: 11,
     fontWeight:'bold'
  },
  moneyIcon:{
    color: "#FFD700",
    fontSize:15,
    marginRight:10
  },
  payment:{
    marginTop:10,
  },
  jobsCardText:{
    color: colorPrimary,
    
    fontFamily: 'OpenSans-Regular',
    fontSize: 15,
    fontWeight:'bold'
  },
  jobDescriptionText:{
    color: "#525252",
    fontSize: 11,
    fontFamily: 'OpenSans-Regular',
  },
  jobFooter:{
    flex:1,
    flexDirection:"row",
    padding: 10,
    alignItems: 'flex-end',
    width: "100%",

    textAlign: "right"
  },
  historyCardText:{
    marginLeft:5,
    color: colorPrimary,
    textAlign:"right",
    fontFamily: 'OpenSans-Regular',
    fontSize: 11,
    fontWeight:'bold',
    flex:1,
    flexDirection:"row",
    alignItems: 'flex-end',
  },
  jobCard :{
    flex:1,
    flexDirection:"row",
    backgroundColor: colorWhite,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
    paddingBottom: 0,
    borderRadius: 8,
    borderBottomWidth:1,
    borderBottomColor: colorPrimary,
    alignItems:"center"
  },
  cardHeading:{
    color: colorWhite,
    fontFamily: 'OpenSans-Regular',
    fontSize: 15,
    fontWeight:'bold',
    marginBottom: 5
  },
  acceptStatusText:{
    borderRadius: 50,
    borderColor: '#22d924',
    borderWidth:1,
    marginRight:10,
    backgroundColor: '#22d924',
    color: colorWhite
  },
  jobViewOuterBox:{
    paddingTop:20
  }


})

export { buttons, text, styles, cardStyle } 