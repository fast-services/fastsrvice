import React from 'react';

//Import all required component
import { View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
const NavigationDrawerHeaderRight = props => {
  const toggleDrawer = () => {
    props.navigationProps.toggleDrawer();
  };

  return (
    <View style={{ flexDirection: 'row' }}>
      <TouchableOpacity onPress={toggleDrawer}>

        <Icon name='bell' size={20} style={{ width: 25, height: 25, marginLeft: 15, marginTop: 10, color :'#fff' }}/>
        
      </TouchableOpacity>
       <TouchableOpacity onPress={toggleDrawer}>
        <Icon name='user-secret' size={20} style={{ width: 25, height: 25, marginLeft: 15, marginTop:10,color :'#fff' }}/>
        
      </TouchableOpacity>
    </View>
  );
};
export default NavigationDrawerHeaderRight;