import React from 'react'
import { View, Image } from 'react-native'

const Logo = (props) => (
    <View>
        <Image
            style={{
                    width: '50%',
                   
                }}
                source={require('../../../assets/images/logo.png')} />
    </View>
)

export default Logo