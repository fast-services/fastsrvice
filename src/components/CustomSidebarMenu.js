import React , { useState }  from 'react';
import axios, {setClientToken } from '../server/apis'
//Import all required component
import { View, StyleSheet, Text, Alert, Switch } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
const CustomSidebarMenu = props => {
  let items = [
    {
      navOptionName: 'CHATS',
      screenToNavigate: 'ProfileScreen',
      icons : 'comments'
    },
    {
      navOptionName: 'HISTORY',
      screenToNavigate: 'historyScreen',
      icons: 'history'
    },
    
    {
      navOptionName: 'REQUEST',
      screenToNavigate: 'HomeScreen',
      icons: 'pencil-square'
    },
    {
      navOptionName: 'JOBS',
      screenToNavigate: 'JobScreen',
      icons: 'search-plus'
    },
    {
      navOptionName: 'UPGRADE TO PRO',
      screenToNavigate: 'HomeScreen',
      icons: 'arrow-circle-up'
    },
    {
      navOptionName: 'HELP',
      screenToNavigate: 'SettingsScreen',
      icons: 'handshake-o'
    },
    {
      navOptionName: 'LOGOUT',
      screenToNavigate: 'logout',
      icons: 'sign-out'
    },
  ];

  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled(previousState => !previousState);

  const closeDrawer = ()=>{
    console.log("Close Drawer press")
    
    props.navigation.toggleDrawer();
  }

  const gotoEdit =()=>{
    console.log("goto Edit is press")
    props.navigation.toggleDrawer();
    props.navigation.navigate('ProfileScreen');
  }

  const handleClick = (index, screenToNavigate) => {
    if (screenToNavigate == 'logout') {
      props.navigation.toggleDrawer();
      Alert.alert(
        'Logout',
        'Are you sure? You want to logout?',
        [
          {
            text: 'Cancel',
            onPress: () => {
              return null;
            },
          },
          {
            text: 'Confirm',
            onPress: async() => {
           //let  token = await AsyncStorage.getItem('token')
           //const response = await axios.post(`/logout`)
         //  console.log(response)
          //  console.log("token", response.data )
              AsyncStorage.clear();
              props.navigation.navigate('Auth');
              console.log('logout');
              
            },
          },
        ],
        { cancelable: false }
      );
    } else {
      props.navigation.toggleDrawer();
      global.currentScreenIndex = screenToNavigate;
      props.navigation.navigate(screenToNavigate);
    }
    console.log(global.currentScreenIndex)
  };
  return (
    <View style={stylesSidebar.sideMenuContainer}>
      <View style={stylesSidebar.profileHeader}>
        <View style={stylesSidebar.profileHeaderPicCircle}>
        
          <Text style={{ fontSize: 25, color: '#307ecc', fontFamily: 'OpenSans-Regular' }}>
            {'About React'.charAt(0)}
          </Text>
        </View>
       
        <View>
       
          <Text style={stylesSidebar.profileHeaderText} onPress={()=>gotoEdit()}> YOUR NAME </Text>
          <Text style={stylesSidebar.profileEditText} onPress={()=>gotoEdit()}> EDIT PROFILE <Icon name="pencil-square-o"  size={18} style={{ marginLeft: 10 }}  /></Text>
         <View style={stylesSidebar.statusbox}>
        
           <Switch trackColor={{ false: "#4362ff", true: "#4362ff" }}
        thumbColor={isEnabled ? "#4362bf" : "#f1f1f1"}
        ios_backgroundColor="#4362bf"
        onValueChange={toggleSwitch}
        value={isEnabled} />
         {isEnabled ? <Text style={stylesSidebar.profileEditText}> ONLINE &nbsp;  </Text> : <Text style={stylesSidebar.profileEditText}> OFFLINE </Text> }
       
        </View>
        </View>
        <Icon name="times-circle" onPress={()=>closeDrawer()}  size={18} style={{ marginLeft: 10,color: '#4362bf', marginRight:15, right: 0, position: 'absolute', top: 10,zIndex: 9999 }} onPress={()=>closeDrawer()} />
       
      </View> 
     
      <View style={{ width: '100%', flex: 1 }}>
        {items.map((item, key) => (
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              paddingLeft: 20,
              paddingBottom: 15,
              paddingTop: 15,
              paddingRight: 20,
              color: 'white',
              borderBottomColor:"#fff",
              borderBottomWidth:1,
              backgroundColor:
                global.currentScreenIndex === item.screenToNavigate
                  ? '#fff'
                  : '#4362bf',
            }}
            key={key}
            onStartShouldSetResponder={() =>
              handleClick(key, item.screenToNavigate)
            }>
             <Icon name={item.icons} size={25} style={{ marginLeft: 0,color : global.currentScreenIndex === item.screenToNavigate
                  ? '#4362bf'
                  : '#fff' }}/>
            <Text style={{ fontSize: 15,marginLeft: 10, fontFamily: 'OpenSans-Regular',fontWeight:'bold', color : global.currentScreenIndex === item.screenToNavigate
                  ? '#4362bf'
                  : '#fff' }}>
              {item.navOptionName}
            </Text>
          </View>
        ))}
      </View>
    </View>
  );
};

const stylesSidebar = StyleSheet.create({
  statusbox:{
    flex:1,
     flexDirection: 'row',
    alignItems:'center',
    width: '100%',
    marginTop: 20,
  }, 
  sideMenuContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: '#4362bf',
    fontFamily: 'OpenSans-Regular',
    color: 'white',
  },
  profileHeader: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    padding:30, 
    textAlign: 'center',
    borderBottomColor:"#fff",
              borderBottomWidth:1,
              fontFamily: 'OpenSans-Regular',
  },
  profileHeaderPicCircle: {
    width: 60,
    height: 60,
    borderRadius: 60 / 2,
    color: 'white',
    backgroundColor: '#4362bf',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  profileHeaderText: {
    color: '#4362bf',
    alignSelf: 'center',
    paddingHorizontal: 10,
    fontWeight: 'bold',
    fontFamily: 'OpenSans-Regular',
    fontSize:22,
  },
  profileEditText:{
    color: '#4362bf',
    alignSelf: 'center',
    paddingHorizontal: 10,
    fontWeight: 'bold',
    fontFamily: 'OpenSans-Regular',
    fontSize:15,
  },
  profileHeaderLine: {
    height: 1,
    marginHorizontal: 20,
    backgroundColor: '#e2e2e2',
    marginTop: 15,
    marginBottom: 10,
  },
});
export default CustomSidebarMenu;