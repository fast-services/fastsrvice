import React from 'react';

//Import all required component
import { View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
const NavigationDrawerHeader = props => {
  const toggleDrawer = () => {
    props.navigationProps.toggleDrawer();
  };

  return (
    <View style={{ flexDirection: 'row' }}>
      <TouchableOpacity onPress={toggleDrawer}>
        {/* <Image
          source={{
            uri:
              'https://raw.githubusercontent.com/AboutReact/sampleresource/master/drawerWhite.png',
          }}
          style={{ width: 25, height: 25, marginLeft: 5 }}
        /> */}
        <Icon name='bars' size={20} style={{ width: 25, height: 25, marginLeft: 15,color :'#fff' }}/>
        
      </TouchableOpacity>
    </View>
  );
};
export default NavigationDrawerHeader;