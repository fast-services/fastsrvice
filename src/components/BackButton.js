import React from 'react';

//Import all required component
import { View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
const BackButton = props => {
  const toggleDrawer = () => {
      //console.log(props.navigationProps)
    props.navigationProps.goBack();
  };

  return (
    <View style={{ flexDirection: 'row' }}>
      <TouchableOpacity onPress={()=>toggleDrawer()}>
        <Icon name='angle-left' size={20} style={{ width: 25, height: 25, marginLeft: 15,color :'#fff' }}/>
        
      </TouchableOpacity>
    </View>
  );
};
export default BackButton;